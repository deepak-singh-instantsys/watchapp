//
//  ExtensionDelegate.swift
//  WatchApp Extension
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import WatchKit
import WatchConnectivity

let KwatchDelegate = WKExtension.shared().delegate as? ExtensionDelegate


class ExtensionDelegate: NSObject, WKExtensionDelegate {

   public var isSynchInProcess = false
   
    public var isSensorInitiated = false
    public var resignActiveTime: Date?
    public var becomeActiveTime: Date?
    public weak var currentInterFace: WKInterfaceController?
    
    var session: WCSession?
    
    func applicationDidFinishLaunching() {
    
        self.requestForLocationAccess()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            
             self.setupWCSession()
             DataProcessor.shared.start()
        }
    }

    func applicationDidBecomeActive() {
    
        self.becomeActiveTime = Date()
        
        if isSensorInitiated && self.currentInterFace != nil {
            
            if let controller = self.currentInterFace as? StartAllInterfaceController, self.becomeActiveTime != nil && self.resignActiveTime != nil, self.resignActiveTime! < self.becomeActiveTime! {
             controller.startFetchPedometer(fromDate: self.resignActiveTime!, toDate: self.becomeActiveTime!)
            }
        }
    }

    func applicationWillResignActive() {
        
        self.resignActiveTime = Date()
        
        
      /*  ProcessInfo.processInfo.performExpiringActivity(withReason: "Reson") { (expired) in
            if !expired {
         
                self.trackActivityName()
            } else {
                print("Background expired")
            }
        }*/
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
            
                backgroundTask.setTaskCompletedWithSnapshot(false)
            
                break
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
}

extension ExtensionDelegate {
    //MARK: BackGround Execution
    
    private func trackActivityName() {
        
        PedometerManager.shared.getCurrentActivity { [weak self] (activity) in
            if self == nil {
                PedometerManager.shared.stopUserActivity()
            }
            if activity != nil {
                if activity != nil {
                    
                    print("Background Activity=",activity)
                    
                }
            }
        }
    }
    
    private func trackActivityData() {
        
        PedometerManager.shared.getPedometerDataFromDate(fromDate: self.resignActiveTime!, toDate: Date()) {  (data, error, errorMsg)  in
            
          
        
            
            if(error == nil && data != nil) {
                
                if let count =  data?.numberOfSteps {
                    
                    let steps = String.init(format: "%d",count.int64Value)
                    print("BackGround Steps=",steps)
                }
                
                if let distance = data?.distance {
                    
                    let roundDis = round(distance.doubleValue)
                    let dis = String.init(format: "%.3f",roundDis)
                    print("BackGround Distance=",dis)
                    
                }
                
                
                if let pace = data?.currentPace {
                    print("BackGround Pace=",pace)
                    
                }
                
                
                if let cadence  = data?.currentCadence {
                    print("BackGround candence=",cadence)
                    
                }
                
                
                if let ascend = data!.floorsAscended {
                    
                }
                
                if let desc = data!.floorsDescended {
                    
                }
            }
        }
    }
}

extension ExtensionDelegate {
    
    //MARK: private
    
    private func setupWCSession() {
        
        if WCSession.isSupported() {
            
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        }
    }
    
    private func requestForLocationAccess() {
        
        LocationManager.shared.getUserCurrentLocation(onSucessBlok: { (lat, long) in
            
            
        }, onFailedBlok: {
            
        })
    }
    
}

extension ExtensionDelegate {
    
    //MARK: Public
    public func sendDictInfo(dictInfo: Dictionary<String, Any>) {
        
        self.session?.sendMessage(dictInfo, replyHandler: { (dictReply) in
            print("Replay=",dictReply)
        }, errorHandler: { (error) in
            print(error.localizedDescription)
        })
    }
    
    public func sendApplicationContext(dictInfo: Dictionary<String, Any>) {
        
        do {
            _ = try self.session?.updateApplicationContext(dictInfo)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

extension ExtensionDelegate : WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if activationState == .activated {
            print("counter part app is active")
        } else if activationState == .notActivated {
            print("counter part app is not active")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("didReceiveMessage")
        
        if let type = message[KMESSAGE_TYPE] as? String {
            
            if type == KMESSAGE_TYPE_AUTH  {
                
                let userName = CommonHelper.convertToString(obj: message[KUSERNAME])
                let password = CommonHelper.convertToString(obj: message[KPASSWORD])
                if CommonHelper.isStringValid(string: userName) && CommonHelper.isStringValid(string: password) {
                    
                    KUserDefault.setValue(userName, forKey: KUSERNAME)
                    KUserDefault.setValue(password, forKey: KPASSWORD)
                    KUserDefault.synchronize()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: KNotification_Auth_Received), object: message)
                }
            }
        }
    }
}
