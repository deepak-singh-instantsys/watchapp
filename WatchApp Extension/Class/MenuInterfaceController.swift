//
//  MenuInterfaceController.swift
//  WatchApp Extension
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import WatchKit
import Foundation

let KSharedWatchDelegate = WKExtension.shared().delegate as! ExtensionDelegate


class MenuInterfaceController: WKInterfaceController {

    @IBOutlet var btnSensor: WKInterfaceButton!
    @IBOutlet var btnAuth: WKInterfaceButton!
    
    var dictInfo = Dictionary<String, Any>()
    var dictInfoPrevious = Dictionary<String, Any>()
    var isAuthenticated = false

    //MARK: Life Cycle
    
    override func awake(withContext context: Any?) {
        
        super.awake(withContext: context)
        self.addObservers()
        
        
    }

    override func willActivate() {
        super.willActivate()
        
        KSharedWatchDelegate.currentInterFace  = self
        KSharedWatchDelegate.isSensorInitiated = false
        
        HealthKitManager.sharedInstance.requestHealthKitPermissionsWithCompletion { (success, error) in
            if success {
                print("Healkit permission allowed")
            } else {
                print("Healkit permission not allowed")
            }
        }
        
        self.checkAuth()
    }

    override func didDeactivate() {
        super.didDeactivate()
    }
    
    deinit {
       
    }
    //MARK: IBActions
    
    @IBAction func tapActivity() {
        
        DispatchQueue.main.async(execute: {
            
            self.pushController(withName: "StartAllInterfaceController", context: nil)
        })
    }
    
    @IBAction func tapAskForAuthenticate() {
     
        didReceiveAuth()
    }
}

extension MenuInterfaceController {
    
    //MARK: Public
    @objc public func didReceiveAuth() {
        
        DispatchQueue.main.async(execute: {
            
            self.checkAuth()
        })
    }
}

extension MenuInterfaceController {
    
    //MARK: Private
    
    private func addObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveAuth), name: NSNotification.Name(rawValue: KNotification_Auth_Received), object: nil)
    }
    
    private func removeObservers() {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: KNotification_Auth_Received), object: nil)
    }
    
    private func checkAuth() {
        
        let userName = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KUSERNAME))
        let password = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KPASSWORD))
        if CommonHelper.isStringValid(string: userName) && CommonHelper.isStringValid(string: password) {
          
            self.isAuthenticated = true
        } else {
            
            self.isAuthenticated = false
        }
        self.authStatus()
    }
    
    private func authStatus() {
        
        if isAuthenticated {
            
            self.btnAuth.setHidden(true)
            self.btnSensor.setHidden(false)
            
                    //Send Auth Ack to iphone app
            let dictInfo = [KMESSAGE_TYPE : KMESSAGE_TYPE_AUTHACK]
            KSharedWatchDelegate.sendDictInfo(dictInfo: dictInfo)
            
        } else {
            
            let dictInfo = [KMESSAGE_TYPE : KMESSAGE_TYPE_AUTH_REQUEST]
            KSharedWatchDelegate.sendDictInfo(dictInfo: dictInfo)
            self.btnAuth.setHidden(false)
            self.btnSensor.setHidden(true)
        }
    }
    
    private func startSensor(){
        
        CloudKitManager.shared.checkLoginStatus { (success) in
            
            if success {
            
                DispatchQueue.main.async(execute: {
                    
                    self.pushController(withName: "StartAllInterfaceController", context: nil)
                })
            } else {
                
                let action1 = WKAlertAction.init(title: "Ok", style:.cancel) {
                    
                }
                
                DispatchQueue.main.async(execute: {
                    
                      self.presentAlert(withTitle: "iCloud Login", message: "Please login to iCloud in your iPhone.", preferredStyle:.actionSheet, actions: [action1])
                    })
            }
        }
    }
}

    


