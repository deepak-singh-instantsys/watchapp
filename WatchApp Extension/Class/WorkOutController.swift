
//
//  WorkOutController.swift
//  WatchApp Extension
//
//  Created by Deepak on 28/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import WatchKit
import HealthKit

class WorkOutController: WKInterfaceController {
    
    
    @IBOutlet var lblTimer: WKInterfaceTimer!
    @IBOutlet var lblCurrentSessionRecord: WKInterfaceLabel!

    var surroundingTemp = ""
    var dictglobalInfo = Dictionary<String, Any>()
    var activityName = ""
    var isCmRecoderFetch = false
    var lastRecodDate = Date()
    var fromDate = Date()
    var currentSessionRecord = 0
    
    
    var workout: WorkOut?
    var recorder: Recorder?
    var motionManager: MotionManager?
    
    var distanceTravelled = 0.0
    var steps  = 0.0
    
    override func awake(withContext context: Any?) {
          super.awake(withContext: context)
    
        KSharedWatchDelegate.currentInterFace  = self
        KSharedWatchDelegate.isSensorInitiated = true
        motionManager = MotionManager()
        recorder = Recorder()
        starAllSensors()
    }
    
    override func willActivate() {
         super.willActivate()
        
    }
    
    override func didDeactivate() {
         super.didDeactivate()
    }
    
    deinit {
        
        workout?.stopWorkout()
        lblTimer.stop()
        stopAllSensors()
        print("De-Initialized")
    }
}

extension WorkOutController {
    
    private func starAllSensors() {
    
        startRecoding()
        startWorkOut()
        trackActivityName()
        getUserLocation()
        startBarometer()
        trackActivityData()
        startAccelerometer()
    }
    
    
    private func stopAllSensors() {
        
        HealthKitManager.sharedInstance.stopWorkout()
        self.motionManager?.stopMotionManager()
        PedometerManager.shared.stopUserActivity()
    }
    

    //Called
    private func trackActivityName() {
        
        PedometerManager.shared.getCurrentActivity { [weak self] (activity) in
            if self == nil {
                PedometerManager.shared.stopUserActivity()
            }
            if activity != nil {
                if activity != nil {
                    self?.activityName = activity!
                }
            }
        }
    }
    
    //called
    private func startAccelerometer() {
    
        self.motionManager?.getDeviceMotionData { [weak self] (deviceMotion, error, errorMsg) in
            
            let timeStamp = CommonHelper.getCurrentTimeStamp()
            
            self?.dictglobalInfo[KTimeStamp] = timeStamp
            
            if deviceMotion != nil && error == nil {
                
                if (deviceMotion?.heading) != nil {
                    
                    let angle = deviceMotion?.heading
                    let headingAngle = String.init(format: "%.3f", angle!)
                    
                    self?.fillInfoToDictionary(value:headingAngle, key: KHeadingAngle)
                }
                
                if let gravity = deviceMotion?.gravity {
                    
                    //// how much is it rotated around the z axis
                    
                    let angle = atan2(gravity.y, gravity.x) + Double.pi / 2
                    let angleDegrees = angle * 180.0 / .pi
                    self?.fillInfoToDictionary(value:String.init(format: "%.5f", angleDegrees), key: KRotationAngle)
                }
                
                if let acceleration = deviceMotion?.userAcceleration {
                    
                    
                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.x), key: KxUser_acceleration)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.y), key: KyUser_acceleration)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.z), key: KzUser_acceleration)
                }
                
                if let rotationRate = deviceMotion?.rotationRate {
                    
                    
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.x), key: KxrotationRate)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.y), key: KyrotationRate)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.z), key: KzrotationRate)
                    
                }
                
                if let attitude = deviceMotion?.attitude {
                    
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.x), key: KxQuaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.y), key: KyQuaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.z), key: KzQuaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.w), key: KwQuaternion)
                }
            }
            self?.sendData()
        }
    }
    
    //Called
    private func trackActivityData() {

        let currentDate = Date.init()
        var isDataReceived = false
        PedometerManager.shared.getPedometerDataFromDate(fromDate: fromDate, toDate: currentDate) { [weak self] (data, error, errorMsg)  in
            
            if isDataReceived == true {
                return
            }
            isDataReceived = true
            
            let timeStamp = CommonHelper.getCurrentTimeStamp()
            self?.dictglobalInfo[KTimeStamp] = timeStamp
            
            if(error == nil && data != nil) {
                
                if let count =  data?.numberOfSteps {
                    
                    let steps = String.init(format: "%d",count.int64Value)
                    self?.fillInfoToDictionary(value: steps, key: KSteps)
                }
                
                if let distance = data?.distance {
                    
                    let roundDis = round(distance.doubleValue)
                    let dis = String.init(format: "%.3f",roundDis)
                    self?.fillInfoToDictionary(value: dis, key: KDistance)
                }
                
                
                if let pace = data?.currentPace {
                    
                    self?.fillInfoToDictionary(value: String(describing: pace), key: KPace)
                }
                
                if let cadence  = data?.currentCadence {
                    
                    self?.fillInfoToDictionary(value: String(describing: cadence), key: KCadence)
                }
                
                if let ascend = data!.floorsAscended {
                    
                    self?.fillInfoToDictionary(value: String(describing: ascend), key: KFlightAsc)
                }
                
                if let desc = data!.floorsDescended {
                    
                    self?.fillInfoToDictionary(value: String(describing: desc), key: KFlightdsc)
                }
                
                if let activity = self?.activityName {
                    
                    self?.fillInfoToDictionary(value: activity, key: kActivity)
                }
                
                self?.sendData()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) { [weak self] in
            
            self?.trackActivityData()
        }
    }
    
    //Called
    func startMovementDetection(){

        if isCmRecoderFetch {
            return
        }
        isCmRecoderFetch = true
        let now = Date()
       
        print("Recorder-Enter")
        
        recorder?.getRecords(fromDate: lastRecodDate, toDate: now, onCompletion: { (arrCMRecorderData) in
            if arrCMRecorderData.count > 0 {
                
                for data in arrCMRecorderData {
                
                    let x:String = NSString(format: "%.5f", (data.acceleration.x)) as String
                    let y:String = NSString(format: "%.5f", (data.acceleration.y)) as String
                    let z:String = NSString(format: "%.5f", (data.acceleration.z)) as String
                    let timeStamp = CommonHelper.getCurrentTimeStamp()
                    
                    
                    let dict: Dictionary<String, Any> = [KxAccel_Accelerometer : x, KyAccel_Accelerometer : y, KzAccel_Accelerometer : z, KTimeStamp : timeStamp]
                    sendData(dictInfo: dict)
                }
                self.lastRecodDate = now
                
            }
            
            print("Recorder-Exit")
            isCmRecoderFetch = false
        })
    }
    
    //Called
    private func startWorkOut() {
        
        lblTimer.start()
        workout = WorkOut()
        workout?.startWorout { [weak self] (arrSample, quantityTypeIdentifier) in
            
            guard let samples = arrSample as? [HKQuantitySample] else {
                
                return
            }
            
            for sample in samples {
                
                if quantityTypeIdentifier == HKQuantityTypeIdentifier.heartRate {
                    
                    let heartRateUnit = HKUnit(from: "count/min")
                    let bpm = sample.quantity.doubleValue(for: heartRateUnit)
                    
                    let timeStamp = CommonHelper.getCurrentTimeStamp()
                    var  dict = Dictionary<String, Any>()
                    dict[KTimeStamp] = timeStamp
                    dict[KHeartRate] = (String(describing: bpm))
                    self?.sendData(dictInfo: dict)
                    
                    
                    print("Heart=\(bpm)")
                    
                } /*else  if quantityTypeIdentifier == HKQuantityTypeIdentifier.distanceWalkingRunning {
                    
                    let meter = HKUnit.meter()
                    let distance = sample.quantity.doubleValue(for: meter)
                    
                     
                    if  self?.distanceTravelled != nil {
                        
                        self?.distanceTravelled = (self?.distanceTravelled)! + distance
                        print("Distance Travelled=\(String(describing: self?.distanceTravelled))")
                        
                        let timeStamp = CommonHelper.getCurrentTimeStamp()
                        var  dict = Dictionary<String, Any>()
                        dict[KTimeStamp] = timeStamp
                        dict[KDistance] = self?.distanceTravelled
                        self?.sendData(dictInfo: dict)
                    }
                    
                } else  if quantityTypeIdentifier == HKQuantityTypeIdentifier.stepCount {
                    
                    let countUnit = HKUnit.count()
                    let step = sample.quantity.doubleValue(for: countUnit)
                    self?.steps += step
                    print("steps=\(String(describing: self?.steps))")
                    
                    
                    let timeStamp = CommonHelper.getCurrentTimeStamp()
                    var  dict = Dictionary<String, Any>()
                    dict[KTimeStamp] = timeStamp
                    dict[KSteps] = self?.steps
                    self?.sendData(dictInfo: dict)
                }*/
            }
            
             self?.startMovementDetection()
        }
    }
    
    //Called
    private func startRecoding() {
        
        recorder?.startRecordingForNext(minutes: 60)
    }

    //Called
    private func getUserLocation() {
        
        LocationManager.shared.getUserCurrentLocation(onSucessBlok: { [weak self] (lat, long) in
            
            if CommonHelper.isStringValid(string: lat) && CommonHelper.isStringValid(string: long) {
                
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[KTimeStamp] = timeStamp
                
                self?.fillInfoToDictionary(value:lat, key: KLatitude)
                self?.fillInfoToDictionary(value:long, key: KLongitude)
                self?.fillInfoToDictionary(value:(self?.surroundingTemp)!, key: KTemperature)
                self?.sendData()
                self?.surroundingTempreture(lat: lat, long: long)
            }
            
            }, onFailedBlok: { [weak self] in
                
                
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5*60)  { [weak self] in
            
            self?.getUserLocation()
            
        }
    }
    
    //Called
    private func startBarometer() {
        
        AltimeterManager.shared.getAltimeterData { [weak self] (data, error, msg) in
            
            if error == nil && data != nil {
                
                let altitude = String.init(format: "%.04f", data!.relativeAltitude.floatValue)
                let presure  = String.init(format: "%.04f", data!.pressure.floatValue)
                
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[KTimeStamp] = timeStamp
                
                self?.fillInfoToDictionary(value:altitude, key: kAltitude)
                self?.fillInfoToDictionary(value:presure, key: kPresure)
                self?.sendData()
            }
            else {
                
                AltimeterManager.shared.stopAltimeter()
                self?.fillInfoToDictionary(value:"", key: kAltitude)
                self?.fillInfoToDictionary(value:"", key: kPresure)
            }
        }
    }
    
    
    //called
    private func surroundingTempreture(lat: String, long: String) {
        
        let appId   = "95eeeb78c1e89b3b7a87cee53d695056"
        let url = String.init(format: "https://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=%@",lat,long, appId)
        APIClient.shared.requestGetAPI(apiURL: url, httpMethoType: .GET) { (dictData, error) in
            
            if error == nil {
                
                guard let  dictMain = dictData!["main"] as? Dictionary<String, Any>, let temp = CommonHelper.convertToString(obj: dictMain["temp"]) as? String else {
                    return
                }
                
                if CommonHelper.isStringValid(string: temp) {
                    
                    let intTemp = Float(temp)
                    if intTemp != nil {
                        
                        let celsius = intTemp! - 273
                        self.surroundingTemp = String(celsius)
                    }
                }
            }
        }
    }

    private func sendData(dictInfo: Dictionary<String, Any>) {
        
        currentSessionRecord += 1
        lblCurrentSessionRecord.setText(String(currentSessionRecord))
        DataProcessor.shared.didReceiveDataFromSensors(dicInfo: dictInfo)
    }
    
    
    private func sendData() {
        
        currentSessionRecord += 1
        lblCurrentSessionRecord.setText(String(currentSessionRecord))
        DataProcessor.shared.didReceiveDataFromSensors(dicInfo: self.dictglobalInfo)
        self.removekeysData()
    }
    
    private func removekeysData() {
        
        self.dictglobalInfo.removeValue(forKey: kActivity)
        self.dictglobalInfo.removeValue(forKey: KSteps)
        self.dictglobalInfo.removeValue(forKey: KDistance)
        self.dictglobalInfo.removeValue(forKey: KPace)
        self.dictglobalInfo.removeValue(forKey: KCadence)
        self.dictglobalInfo.removeValue(forKey: KFlightAsc)
        self.dictglobalInfo.removeValue(forKey: KFlightdsc)
    }
    
    private func fillInfoToDictionary(value: String, key: String) {
        
        self.dictglobalInfo[key] = value
    }
}
