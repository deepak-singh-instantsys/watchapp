//
//  StartAllInterfaceController.swift
//  WatchApp Extension
//
//  Created by Deepak on 11/17/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import HealthKit
import WatchKit
import Foundation
import CoreData
import WatchConnectivity


class StartAllInterfaceController: WKInterfaceController {
    
    @IBOutlet var btnStatus: WKInterfaceButton!
    @IBOutlet var lblTimer: WKInterfaceTimer!
    @IBOutlet var lblCurrentSessionRecord: WKInterfaceLabel!
    
    var arrCMSensorRecorder = [Dictionary<String, Any>]()
    var isCmRecoderFetch = false
    var lastRecodDate = Date()

    var currentSessionRecord = 0
    var dictglobalInfo = Dictionary<String, Any>()
    var startDate = Date.init()
    var activityName = " "
    var surroundingTemp = " "
    var arrData = [Dictionary<String, Any>]()
    var recorderDate = Date.init()
    private var locationTimer : DispatchSourceTimer?

    var workout: WorkOut?
    var recorder: Recorder?
    var motionManager: MotionManager?
    
            //MARK: Life Cycles
    override func awake(withContext context: Any?) {
        
        super.awake(withContext: context)
        self.motionManager = MotionManager()
        self.startAll()
    }

    override func willActivate() {
        
        super.willActivate()
        
        KSharedWatchDelegate.currentInterFace  = self
        KSharedWatchDelegate.isSensorInitiated = true
    }
    
    override func didDeactivate() {
        
        super.didDeactivate()
    }
    
    deinit {
        
        self.stopAllSensors()
        print("Interface controller de-allocated")
    }
    
    @IBAction func tapStopSensors() {
        
         self.stopAllSensors()
    }
}

extension StartAllInterfaceController {
    //MARK: Public
    
    public func startFetchPedometer(fromDate: Date, toDate: Date) {
    
        
        let fromDateTimeStamp = CommonHelper.getTimeStamp(date: fromDate)
        let toDateTimeStamp = CommonHelper.getTimeStamp(date: toDate)
    

        if toDateTimeStamp - fromDateTimeStamp >= 60 {
         return
        }
    }
}


extension StartAllInterfaceController {
    
    //MARK: Private
    private func startAll() {
        
        let mainQueue = OperationQueue.main
        let blockOp = BlockOperation()
        blockOp.addExecutionBlock {
            self.startTimer()
            self.trackActivityName()
            self.trackActivityData()
            self.startBarometer()
            self.getHeadAngle()
            self.startAccelerometer()
            self.getGyroData()
            self.getUserLocation()
            self.startRecoding()
            self.startWorkOut()
            self.getAccelerometerData()
        }
        mainQueue.addOperation(blockOp)
    }
    
    private func requestForHealthKit() {
        
        HealthKitManager.sharedInstance.requestHealthKitPermissionsWithCompletion { (success, error) in
            
        }
    }
    
    private func startTimer() {
    
        self.lblTimer.start()
    }
    
    private func trackActivityName() {
        
        PedometerManager.shared.getCurrentActivity { [weak self] (activity) in
            if self == nil {
                PedometerManager.shared.stopUserActivity()
            }
            if activity != nil {
                if activity != nil {
    
                    self?.activityName = activity!
                }
            }
        }
    }
    
    private func trackActivityData() {

        let currentDate = Date.init()
        var isDataReceived = false
        PedometerManager.shared.getPedometerDataFromDate(fromDate: self.startDate, toDate: currentDate) { [weak self] (data, error, errorMsg)  in
            
            if isDataReceived == true {
                return
            }
            isDataReceived = true
            
            let timeStamp = CommonHelper.getCurrentTimeStamp()
            self?.dictglobalInfo[Ktime_stamp] = timeStamp
            
                if(error == nil && data != nil) {
                
                if let count =  data?.numberOfSteps {
                    
                    let steps = String.init(format: "%d",count.int64Value)
                    self?.fillInfoToDictionary(value: steps, key: Ksteps)
                }
                
                if let distance = data?.distance {
                    
                    let roundDis = round(distance.doubleValue)
                    let dis = String.init(format: "%.3f",roundDis)
                    self?.fillInfoToDictionary(value: dis, key: Kdistance)
                }
                
                
                if let pace = data?.currentPace {
                    
                    self?.fillInfoToDictionary(value: String(describing: pace), key: Kpace)
                }
                
                if let cadence  = data?.currentCadence {
                    
                    self?.fillInfoToDictionary(value: String(describing: cadence), key: Kcadence)
                }
                
                if let ascend = data!.floorsAscended {
                    
                    self?.fillInfoToDictionary(value: String(describing: ascend), key: Kflight_asc)
                }
                
                if let desc = data!.floorsDescended {
                    
                    self?.fillInfoToDictionary(value: String(describing: desc), key: Kflight_desc)
                }
                
                if let activity = self?.activityName {
                    
                    self?.fillInfoToDictionary(value: activity, key: Kactivity)
                }
                
                self?.sendData()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) { [weak self] in
            
            self?.trackActivityData()
        }
    }
    
    private func startPedometer() {
        
        PedometerManager.shared.getPedometerData { [weak self] (data, error) in
            
            if(error == nil && data != nil) {
                
                if let count =  data?.numberOfSteps {
                    
                    let steps = String.init(format: "%d",count.int64Value)
                    print("Forground Pedo Step=",steps)

                    self?.fillInfoToDictionary(value: steps, key: Ksteps)
                }
                
                if let distance = data?.distance {
                    
                    let roundDis = round(distance.doubleValue)
                    let dis = String.init(format: "%.3f",roundDis)
                    self?.fillInfoToDictionary(value: dis, key: Kdistance)
                }
                
                
                if let pace = data?.currentPace {
                    
                    self?.fillInfoToDictionary(value: String(describing: pace), key: Kpace)
                }
                
                
                if let cadence  = data?.currentCadence {
                    
                    self?.fillInfoToDictionary(value: String(describing: cadence), key: Kcadence)
                }
                
                
                if let ascend = data!.floorsAscended {
                    
                    self?.fillInfoToDictionary(value: String(describing: ascend), key: Kflight_asc)
                }
                
                if let desc = data!.floorsDescended {
                    
                    self?.fillInfoToDictionary(value: String(describing: desc), key: Kflight_desc)
                }
            }
        }
    }

     private func getUserLocation() {
        
        LocationManager.shared.getUserCurrentLocation(onSucessBlok: { [weak self] (lat, long) in
            
            if CommonHelper.isStringValid(string: lat) && CommonHelper.isStringValid(string: long) {
               
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[Ktime_stamp] = timeStamp
                
                self?.fillInfoToDictionary(value:lat, key: Klatitude)
                self?.fillInfoToDictionary(value:long, key: Klongitude)
                self?.fillInfoToDictionary(value:(self?.surroundingTemp)!, key: Ktemperature)
                self?.sendData()
                self?.surroundingTempreture(lat: lat, long: long)
            }
            
            }, onFailedBlok: { [weak self] in
            
              
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5*60)  { [weak self] in
            
            self?.getUserLocation()
            
        }
    }
    
    private func startBarometer() {
        
        AltimeterManager.shared.getAltimeterData { [weak self] (data, error, msg) in
            
            if error == nil && data != nil {
                
                let altitude = String.init(format: "%.04f", data!.relativeAltitude.floatValue)
                let presure  = String.init(format: "%.04f", data!.pressure.floatValue)
                
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[Ktime_stamp] = timeStamp
                
                self?.fillInfoToDictionary(value:altitude, key: Kaltitude)
                self?.fillInfoToDictionary(value:presure, key: Kpresure)
                self?.sendData()
            }
            
        }
    }
    
    private func getHeadAngle() {
        
        self.motionManager?.getHeading { [weak self] (devicemotion, error, errorMsg) in
            
            if devicemotion != nil && error == nil {
                let angle = devicemotion?.heading
                let headingAngle = String.init(format: "%.5f", angle!)
                self?.fillInfoToDictionary(value:headingAngle, key: Kheading_angle)
            }
        }
    }

    /*private func getHeartRate() {
        
        HealthKitManager.sharedInstance.getHeartRate { [weak self] (heartRate) in
            
            if heartRate != nil {
                
                HealthKitManager.sharedInstance.stopWorkout()
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[KTimeStamp] = timeStamp
                self?.fillInfoToDictionary(value:heartRate!, key: KHeartRate)
                self?.sendData()
            }
            
            HealthKitManager.sharedInstance.stopWorkout()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5*60)  { [weak self] in
            
            self?.getHeartRate()
        }
    }*/
    
    private func startAccelerometer() {
        
        self.motionManager?.getAccelerometerData { [weak self] (accelerometerData, error, errorMsg)  in
            if error == nil && accelerometerData != nil {
                
                let x:String = NSString(format: "%.5f", (accelerometerData?.acceleration.x)!) as String
                let y:String = NSString(format: "%.5f", (accelerometerData?.acceleration.y)!) as String
                let z:String = NSString(format: "%.5f", (accelerometerData?.acceleration.z)!) as String
                
                let timeStamp = CommonHelper.getCurrentTimeStamp()
                self?.dictglobalInfo[Ktime_stamp] = timeStamp
                self?.fillInfoToDictionary(value:x, key: Kx_acceleration)
                self?.fillInfoToDictionary(value:y, key: Ky_acceleration)
                self?.fillInfoToDictionary(value:z, key: Kz_acceleration)
                self?.sendData()
            }
        }
        
        self.motionManager?.getDeviceMotionData { [weak self] (deviceMotion, error, errorMsg) in
            
            let timeStamp = CommonHelper.getCurrentTimeStamp()
            
            self?.dictglobalInfo[Ktime_stamp] = timeStamp
            
            if deviceMotion != nil && error == nil {
                
                if (deviceMotion?.heading) != nil {
                    
                    let angle = deviceMotion?.heading
                    let headingAngle = String.init(format: "%.3f", angle!)
                   
                    self?.fillInfoToDictionary(value:headingAngle, key: Kheading_angle)
                }
                
                if let gravity = deviceMotion?.gravity {
                  
                    //// how much is it rotated around the z axis

                    let angle = atan2(gravity.y, gravity.x) + Double.pi / 2
                    let angleDegrees = angle * 180.0 / .pi
                   self?.fillInfoToDictionary(value:String.init(format: "%.5f", angleDegrees), key: Krotation_angle)
                }
                
                if let acceleration = deviceMotion?.userAcceleration {
                    

                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.x), key: Kx_user_accel)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.y), key: Ky_user_accel)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", acceleration.z), key: Kz_user_accel)
                }
                
                if let rotationRate = deviceMotion?.rotationRate {
                    
                
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.x), key: Kx_rotationRate)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.y), key: Ky_rotationRate)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.z), key: Kz_rotationRate)
                    
                }
                
                if let attitude = deviceMotion?.attitude {
                    
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.x), key: Kx_quaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.y), key: Ky_quaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.z), key: Kz_quaternion)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", attitude.quaternion.w), key: Kw_quaternion)
                }
            }
            self?.sendData()
        }
    }

    
    private func getGyroData() {
        
     /*  self.motionManager?.startGyroscope { [weak self] (data, error, errorMsg) in
            
            if data != nil && error == nil {
                
                if let rotationRate = data?.rotationRate {
                    
                    let timeStamp = CommonHelper.getCurrentTimeStamp()
                    self?.dictglobalInfo[KTimeStamp] = timeStamp
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.x), key: KXRotationRate_gyro)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.y), key: KYRotationRate_gyro)
                    self?.fillInfoToDictionary(value:String(format: "%.5f", rotationRate.z), key: KZRotationRate_gyro)
                    self?.sendData()
                }
            }
        }*/
    }
    
    private func fillInfoToDictionary(value: String, key: String) {
        
        self.dictglobalInfo[key] = value
    }
    
    private func stopAllSensors() {
        
        //HealthKitManager.sharedInstance.stopWorkout()
        self.motionManager?.stopMotionManager()
        PedometerManager.shared.stopUserActivity()
        workout?.stopWorkout()
    }
    
    private func sendData() {
        
        currentSessionRecord += 1
        lblCurrentSessionRecord.setText(String(currentSessionRecord))

        var isConnected = true
        if let isReachable = KSharedWatchDelegate.session?.isReachable {
            
            if isReachable {
                isConnected = true
                self.btnStatus.setTitle("Reachable")
            
            } else {
                isConnected = false
            }
        }
        
        if isConnected == false {
            
            self.btnStatus.setTitle("Not Reachable")
        }
        
        self.removeBlankKeys()
        let dictItem = ["Item" : self.dictglobalInfo]
        let dictPutRequest = ["PutRequest" : dictItem]
        self.removekeysData()
        DataProcessor.shared.didReceiveDataFromSensors(dicInfo: dictPutRequest)
    }
    
    private func removeBlankKeys() {
        
        for key in self.dictglobalInfo.keys {
            
            let value = CommonHelper.convertToString(obj: self.dictglobalInfo[key])
            if !CommonHelper.isStringValid(string: value) {
                self.dictglobalInfo[key] = "    "
            }
        }
    }
    
    private func removekeysData() {
        
        self.dictglobalInfo.removeValue(forKey: Kactivity)
        self.dictglobalInfo.removeValue(forKey: Ksteps)
        self.dictglobalInfo.removeValue(forKey: Kdistance)
        self.dictglobalInfo.removeValue(forKey: Kpace)
        self.dictglobalInfo.removeValue(forKey: Kcadence)
        self.dictglobalInfo.removeValue(forKey: Kflight_asc)
        self.dictglobalInfo.removeValue(forKey: Kflight_desc)
    }
    

    private func surroundingTempreture(lat: String, long: String) {
        
        let appId   = "95eeeb78c1e89b3b7a87cee53d695056"
        let url = String.init(format: "https://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=%@",lat,long, appId)
        APIClient.shared.requestGetAPI(apiURL: url, httpMethoType: .GET) { (dictData, error) in
            
            if error == nil {
                
                guard let  dictMain = dictData!["main"] as? Dictionary<String, Any>, let temp = CommonHelper.convertToString(obj: dictMain["temp"]) as? String else {
                    return
                }
                
                if CommonHelper.isStringValid(string: temp) {
                    
                    let intTemp = Float(temp)
                    if intTemp != nil {
                        
                        let celsius = intTemp! - 273
                        self.surroundingTemp = String(celsius)
                    }
                }
            }
        }
    }
    
    /************ New Methods are added *************/
    
    
    func getAccelerometerData() {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
//           self?.getRecorderData()
//        }
    }
    private func startWorkOut() {
        
        lblTimer.start()
        workout = WorkOut()
        workout?.startWorout { [weak self] (arrSample, quantityTypeIdentifier) in
            
            guard let samples = arrSample as? [HKQuantitySample] else {
                
                return
            }
            
            for sample in samples {
                
                if quantityTypeIdentifier == HKQuantityTypeIdentifier.heartRate {
                    
                    let heartRateUnit = HKUnit(from: "count/min")
                    let bpm = sample.quantity.doubleValue(for: heartRateUnit)
                    
                    let timeStamp = CommonHelper.getCurrentTimeStamp()
                    self?.dictglobalInfo[Ktime_stamp] = timeStamp
                    self?.dictglobalInfo[Kheart_rate] = (String(describing: bpm))
                    self?.sendData()
                    print("Heart=\(bpm)")
                    
                }
            }
        }
    }
    
    private func startRecoding() {
        
        recorder = Recorder()
        recorder?.startRecordingForNext(minutes: 60)
    }
    
                /****** Not in Use ************************/
    private func findPedometerData_Forground_Background_support() {
        
        let now = Date()
        PedometerManager.shared.getPedometerDataFromDate(fromDate: self.startDate, toDate: now) { [weak self] (data, error, errorMsg)  in
            
            
            if(error == nil && data != nil) {
                
                if let count =  data?.numberOfSteps {
                    
                    let steps = String.init(format: "%d",count.int64Value)
                    print("BG Steps=\(steps)")
                    
                }
                
                if let distance = data?.distance {
                    
                    let roundDis = round(distance.doubleValue)
                    let dis = String.init(format: "%.3f",roundDis)
                    print("BG Distance=\(dis)")
                    
                }
                
                
                if let pace = data?.currentPace {
                    
                    print("BG pace=\(pace)")
                    
                }
                
                if let cadence  = data?.currentCadence {
                    
                    print("BG cadence=\(cadence)")
                    
                }
                
                if let ascend = data!.floorsAscended {
                    
                    print("BG ascend=\(ascend)")
                    
                }
                
                if let desc = data!.floorsDescended {
                    
                    print("BG desc=\(desc)")
                    
                }
                
                if let activity = self?.activityName {
                    
                    print("BG activity=\(activity)")
                    
                }
                
            }
        }
    }
 }


extension StartAllInterfaceController {
    //MARK: New methods
    
    func getRecorderData() {
        
        if isCmRecoderFetch {
            return
        }
        isCmRecoderFetch = true
        
        let now = Date()
        print("Recorder-Enter")
        
        recorder?.getRecords(fromDate: lastRecodDate, toDate: now, onCompletion: {[weak self] (arrCMRecorderData) in
            if arrCMRecorderData.count > 0 {
                
                for data in arrCMRecorderData {
                    
                    let x:String = NSString(format: "%.5f", (data.acceleration.x)) as String
                    let y:String = NSString(format: "%.5f", (data.acceleration.y)) as String
                    let z:String = NSString(format: "%.5f", (data.acceleration.z)) as String
                    
                    
                    let dict = [Kx_acceleration :x, Ky_acceleration : y, Kz_acceleration : z]
                    self?.arrCMSensorRecorder.append(dict)
                }
                self?.lastRecodDate = now
            }
            
            print("Recorder-Exit with total recorder = \(String(describing: self?.arrCMSensorRecorder.count))")
            isCmRecoderFetch = false
        })
    }
}
