//
//  ICloudeLoginCheck.swift
//  WatchApp Extension
//
//  Created by Deepak on 24/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import WatchKit

class ICloudeLoginCheck: WKInterfaceController {
    
    var dictInfo = Dictionary<String, Any>()
    var dictInfoPrevious = Dictionary<String, Any>()
    
    //MARK: Life Cycle
    
    override func awake(withContext context: Any?) {
        
        super.awake(withContext: context)
        
        HealthKitManager.sharedInstance.requestHealthKitPermissionsWithCompletion { (success, error) in
            if success {
                print("Healkit permission allowed")
            } else {
                print("Healkit permission not allowed")
            }
        }
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    //MARK: IBActions
    
    @IBAction func tapActivity() {
        
        CloudKitManager.shared.checkLoginStatus { (success) in
            if success {
                
                DispatchQueue.main.async(execute: {
                    self.pushController(withName: "StartAllInterfaceController", context: nil)

                })
                
            } else {
                
                let action1 = WKAlertAction.init(title: "Ok", style:.cancel) {
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.presentAlert(withTitle: "iCloud Login", message: "Please login to iCloud in your phone.", preferredStyle:.actionSheet, actions: [action1])
                })
                
            }
        }
    }
    
    @IBAction func tapDeleteRecords() {
        let filManager = FileManagerProcessor()
        filManager.removeAllRecords()
    }

}
