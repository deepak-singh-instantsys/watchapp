//
//  Recorder.swift
//  WatchSample
//
//  Created by Deepak on 29/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreMotion

class Recorder: NSObject {

    let recorder = CMSensorRecorder()
    
    override init() {
        super.init()
    }
    
    func startRecordingForNext(minutes: Double) {
        
        if CMSensorRecorder.isAccelerometerRecordingAvailable() {
            
            recorder.recordAccelerometer(forDuration: minutes*60)
        }
    }
    
    func getRecords(fromDate: Date, toDate: Date, onCompletion:(_ arrAccelerometer: [CMRecordedAccelerometerData])->Void) {
        
         var arrAccelerometerData = [CMRecordedAccelerometerData]()
         if CMSensorRecorder.isAccelerometerRecordingAvailable() {
            
           
            if let list =   recorder.accelerometerData(from: fromDate, to: toDate) {
                
                for item in list {
                    if let data = item as? CMRecordedAccelerometerData {
                        
                        arrAccelerometerData.append(data)
                    }
                }
            }
        }
        
         onCompletion(arrAccelerometerData)
    }
}

extension CMSensorDataList: Sequence {
    
    public func makeIterator() -> NSFastEnumerationIterator {
        return NSFastEnumerationIterator(self)
    }
}
