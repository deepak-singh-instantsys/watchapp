//
//  HealthKitManager.swift
//  HealthKitSample
//
//  Created by Deepak on 05/11/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import HealthKit

//enum MyError: Error {case err}
class HealthKitManager: NSObject {
    
    var blockHeartRate: ((_ heartRate: String?)-> Void)?
    
    
    
    public static let sharedInstance = HealthKitManager()
    private let healthStore = HKHealthStore()
   
    private var session : HKWorkoutSession?
    private let heartRateUnit = HKUnit(from: "count/min")
    var currenQuery : HKQuery?
    

    private let activeEnergyBurnedQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
    private let heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    
    
                    //MARK: - HealthKit Permissions
    
    
    private func dataTypesToWrite() -> Set<HKSampleType> {
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let workoutObjectType = HKObjectType.workoutType()
            let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
         let distance = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        
        
        
         let flightsClimbed = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)!
        return  [heartRateQuantityType, workoutObjectType, stepsCount, distance, flightsClimbed]
    }
    
    private func dataTypesToRead() -> Set<HKObjectType> {
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        
        
        let distance = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        let workoutObjectType = HKObjectType.workoutType()
        
         let flightsClimbed = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.flightsClimbed)!
        return  [heartRateQuantityType, workoutObjectType, stepsCount, distance, flightsClimbed]
    }
    
    
   /* private func dataTypesToWrite() -> Set<HKSampleType> {
        
        let dietaryCalorieEnergyType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryEnergyConsumed)!
        let activeEnergyBurnType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let heightType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let weightType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let workoutObjectType = HKObjectType.workoutType()
        
        return [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, workoutObjectType]
        
    
    }
    
    private func dataTypesToRead() -> Set<HKObjectType> {
        
        
       /* let dietaryCalorieEnergyType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryEnergyConsumed)!
        let activeEnergyBurnType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let heightType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let weightType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let birthdayType = HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!
        let biologicalSexType = HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let bodyTemperatureType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature)
        let  activeEnergyBurned = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        let basalBodyTemperature = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalBodyTemperature)
        let bloodPressureSystolic = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)
        
        return [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType, heartRateQuantityType, bodyTemperatureType!, activeEnergyBurned!, basalBodyTemperature!, bloodPressureSystolic!]*/
     
    }*/

}

extension HealthKitManager {
    
    //MARK: Public
    
    public func isHealthKitAvailable() -> Bool {
        
        return HKHealthStore.isHealthDataAvailable()
    }
    
    public func requestHealthKitPermissionsWithCompletion(completion: @escaping (Bool, Error?) -> Void) {
        
        let readTypes = self.dataTypesToRead()
        let shareTypes = self.dataTypesToWrite()
        
        healthStore.requestAuthorization(toShare: shareTypes, read: readTypes) { (success, error) -> Void in
            if !success {
                
                print("Error when requesting authorization: \(String(describing: error))")
            }
            completion(success, error)
        }
    }
    
    
    public func age() -> Int? {
        
        do {
            let dateOfBirth = try self.healthStore.dateOfBirth()
            let now = Date()
            let ageComponents = (Calendar.current as NSCalendar).components(.year, from: dateOfBirth, to: now, options: .wrapComponents)
            
            let usersAge = ageComponents.year!
            return usersAge

        } catch _ {
            
           return nil
        }
    }
    
    public func height(completionHandler:@escaping (_ height: String?)-> Void) {
        
        let heightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        
        // Query to get the user's latest height, if it exists.
        
        self.healthStore.aapl_mostRecentQuantitySampleOfType(heightType, predicate: nil) {mostRecentQuantity, error in
            if mostRecentQuantity == nil {
                
                NSLog("Error - height --- Either an error occured fetching the user's height information or none has been stored yet. In your app, try to handle this gracefully.")
                
                
                DispatchQueue.main.async {
                    
                    completionHandler("Not available")

                }
                
            } else {
                let heightUnit = HKUnit.inch()
                let usersHeight = mostRecentQuantity!.doubleValue(for: heightUnit)
                
                DispatchQueue.main.async {
                    
                    completionHandler(NumberFormatter.localizedString(from: usersHeight as NSNumber, number: NumberFormatter.Style.none))
                }
            }
        }
    }
    
    public func weight(completionHandler:@escaping (_ weight: String?)-> Void) {
        
        let weightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        
        self.healthStore.aapl_mostRecentQuantitySampleOfType(weightType, predicate: nil) {mostRecentQuantity, error in
            if mostRecentQuantity == nil {
                
                NSLog("Either an error occured fetching the user's weight information or none has been stored yet. In your app, try to handle this gracefully.")
                
                DispatchQueue.main.async {
                    
                    completionHandler("Not available")
                }
            } else {
                
                let weightUnit = HKUnit.pound
                let usersWeight = mostRecentQuantity!.doubleValue(for: weightUnit())
                
                DispatchQueue.main.async {
                
                     completionHandler(NumberFormatter.localizedString(from: usersWeight as NSNumber, number: .none))
                
                }
            }
        }
    }
    
    
    public func getHeartRate(onRun:@escaping (_ heartRate: String?)-> Void) {
      self.blockHeartRate = onRun
      self.startWorkout()
    }
    
    
    func getTodaysSteps(fromDate: Date, toDate: Date, completion: @escaping (Double) -> Void) {
        
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                
                print(error?.localizedDescription ?? "N/A")
                completion(resultCount)
                return
            }
            
            
            print("result=\(result)")
            
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.count())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthStore.execute(query)
    }
    
    private func createHeartRateStreamingQuery(_ workoutStartDate: Date) -> HKQuery? {
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return nil }
        let datePredicate = HKQuery.predicateForSamples(withStart: workoutStartDate, end: nil, options: .strictEndDate )
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates:[datePredicate])
        
        
        let heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            self.updateHeartRate(sampleObjects)
        }
        
        heartRateQuery.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in

            self.updateHeartRate(samples)
        }
        return heartRateQuery
    }
    
    private  func updateHeartRate(_ samples: [HKSample]?) {
    
        guard let heartRateSamples = samples as? [HKQuantitySample] else {
            
            self.blockHeartRate?(nil)
            return
        }
        
        DispatchQueue.main.async {
            
            guard let sample = heartRateSamples.first else{return}
            let value = sample.quantity.doubleValue(for: self.heartRateUnit)
            self.blockHeartRate?(String(value))
            self.stopWorkout()
            
        }
    }
    
    
  private  func startWorkout() {
        
        if (session != nil) {
            return
        }
        
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = .crossTraining
        workoutConfiguration.locationType = .indoor
        
        do {
            session = try HKWorkoutSession(configuration: workoutConfiguration)
            session?.delegate = self
        } catch {
            self.couldNotGetHeartRate()
            fatalError("Unable to create the workout session!")
        }
        
        healthStore.start(self.session!)
    }
    
     public func stopWorkout() {
        
        if self.session != nil {
            
            healthStore.end(self.session!)
        }

    }
    
    private func couldNotGetHeartRate() {
        
        self.blockHeartRate?(nil)
        self.stopWorkout()
    }
    
    private  func workoutDidStart(_ date : Date) {
        
        if let query = createHeartRateStreamingQuery(date) {
            self.currenQuery = query
            healthStore.execute(query)
        } else {
            self.couldNotGetHeartRate()
        }
    }
    
    private  func workoutDidEnd(_ date : Date) {
        
        if self.currenQuery != nil {
            
            healthStore.stop(self.currenQuery!)
            session = nil
        }
    }
}




extension HealthKitManager :  HKWorkoutSessionDelegate {
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        
        self.stopWorkout()
    }
    

    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        
        switch toState {
        case .running:
            workoutDidStart(date)
        case .ended:
            workoutDidEnd(date)
        default:
            print("Unexpected state \(toState)")
        }
    }
}

extension HKHealthStore {
    
    // Fetches the single most recent quantity of the specified type.
    func aapl_mostRecentQuantitySampleOfType(_ quantityType: HKQuantityType, predicate: NSPredicate?, completion: ((HKQuantity?, Error?)->Void)?) {
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        // Since we are interested in retrieving the user's latest sample, we sort the samples in descending order, and set the limit to 1. We are not filtering the data, and so the predicate is set to nil.
        let query = HKSampleQuery(sampleType: quantityType, predicate: nil, limit: 1, sortDescriptors: [timeSortDescriptor]) {query, results, error in
            if results == nil {
                completion?(nil, error)
                
                return
            }
            
            if completion != nil {
                // If quantity isn't in the database, return nil in the completion block.
                let quantitySample = results!.first as? HKQuantitySample
                let quantity = quantitySample?.quantity
                
                completion!(quantity, error)
            }
        }
        
        self.execute(query)
    }
}
