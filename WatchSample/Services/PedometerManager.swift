//
//  PedometerManager.swift
//  PedoMeter
//
//  Created by Deepak on 11/6/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreMotion


enum MyError: Error {case err}

class PedometerManager: NSObject {
    
    var pedoMeter: CMPedometer?
    var activityManager: CMMotionActivityManager?
    
    static let shared: PedometerManager = {
       return PedometerManager()
    }()

    private override init() {
        
        super.init()
        self.pedoMeter = CMPedometer()
        self.activityManager = CMMotionActivityManager()
    }
    
    
    public func getPedometerData(onRun:@escaping (_ data: CMPedometerData?, _ error: Error?)-> Void) {
        
        let todya = Date.init()
        
        
        self.pedoMeter?.startUpdates(from: todya) { (data: CMPedometerData!, error) -> Void in
            
            DispatchQueue.main.async(execute: {
                
                onRun(data, error)
            })
        }
    }
    
    public func getPedometerDataFromDate(fromDate: Date, toDate: Date, completionBlock:@escaping (_ data: CMPedometerData?, _ error: Error?, _ errorMsg: String?)-> Void) {
        
        if(CMPedometer.isStepCountingAvailable()) {
            
            self.pedoMeter?.queryPedometerData(from: fromDate, to: toDate) { (data : CMPedometerData!, error) -> Void in
                
                DispatchQueue.main.async(execute: {
                    
                    completionBlock(data, error, "")
                })
            }
        } else {
            
            completionBlock(nil, MyError.self as? Error , "Pedometer Not Available")
        }
    }
    
    
    
    public func getCurrentActivity(onRun:@escaping (_ activity: String?) -> Void) {
        
        if(CMMotionActivityManager.isActivityAvailable()){
            
            let mainQ = OperationQueue.main
            
            self.activityManager?.startActivityUpdates(to: mainQ, withHandler: { (data: CMMotionActivity!) -> Void in
                
                DispatchQueue.main.async(execute: {
                    
                    var action = ""
                    if(data.stationary == true){
                        action = "Stationary"
                    } else if (data.walking == true){
                       action = "Walking"
                    } else if (data.running == true){
                        action = "Running"
                    } else if (data.automotive == true){
                        action = "Automotive"
                    } else if (data.cycling == true){
                        action = "cycling"
                    } else {
                        
                        action = "Stationary"
                    }
                    
                    onRun(action)
                    
                })
            })
        }
    }
    
    public func stopUserActivity() {
        
        DispatchQueue.main.async {
            self.pedoMeter?.stopUpdates()
            self.pedoMeter?.stopEventUpdates()
            self.activityManager?.stopActivityUpdates()
        }
    }
}

//MARK: - Double Units Extension
extension Double {
    
    var km: Double { return self * 1_000.0 }
    var m: Double { return self }
    var cm: Double { return self / 100.0 }
    var mm: Double { return self / 1_000.0 }
    var ft: Double { return self / 3.28084 }
}
