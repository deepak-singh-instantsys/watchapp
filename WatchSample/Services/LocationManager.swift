//
//  LocationManager.swift
//  SwagbucksSDKCode
//
//  Created by Deepak Singh on 18/07/17.
//  Copyright © 2017 Deepak Singh. All rights reserved.
//

import UIKit
import MapKit

/******************************
 * This class is responsible for Fetching Location of Device
 ******************************/

class LocationManager: NSObject, CLLocationManagerDelegate {

    private var clLocationManager: CLLocationManager?
    private var SuccessBlock:((_ latitude: String, _ longitude: String)->Void)?
    private var OnFailedBlock:(()->Void)?
    private var lat: String?
    private var long: String?

    static let shared: LocationManager = {
        let instance = LocationManager()
        return instance
    }()
    
    private override init() {
        super.init()
    }
    

    private  func invokeLocationManager() {
        
        if self.clLocationManager == nil {
            
            self.clLocationManager =  CLLocationManager()
            self.clLocationManager?.delegate = self
        }
        
        self.clLocationManager?.requestWhenInUseAuthorization()
        self.clLocationManager?.startUpdatingLocation()
    }
    
    
    func getUserCurrentLocation(onSucessBlok onSucessBlock:@escaping (_ lat: String, _ long: String)->Void, onFailedBlok onFailedBlock:@escaping ()->Void) ->Void {
        
        self.SuccessBlock = onSucessBlock
        self.OnFailedBlock = onFailedBlock
        self.invokeLocationManager()
    }
    
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     
        self.clLocationManager?.stopUpdatingLocation()
        if locations.count > 0 {
            
            let userLocation = locations[0]
             self.long = String(userLocation.coordinate.longitude);
             self.lat = String(userLocation.coordinate.latitude);
            
            if self.SuccessBlock != nil {
                
                self.SuccessBlock!(self.lat!, self.long!)
                self.SuccessBlock = nil
            }
            
        } else {
            
            if self.OnFailedBlock != nil {
                
                self.OnFailedBlock!()
                self.OnFailedBlock = nil
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if self.OnFailedBlock != nil {
            
            self.OnFailedBlock!()
            self.OnFailedBlock = nil
        }
    }
}
