//
//  CompaseHeadManager.swift
//  Sensors
//
//  Created by Deepak on 11/9/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreLocation
import Darwin
import Foundation


class CompaseHeadManager: NSObject {

   private  let locationManager = CLLocationManager()
    
   private  var locationCallback: ((CLLocation) -> ())? = nil
    private  var watchLocationCallback: ((CLLocation) -> ())? = nil

   private  var headingCallback: ((CLLocationDirection) -> ())? = nil
    
    var latestLocation: CLLocation? = nil
    var yourLocationBearing: CGFloat { return latestLocation?.bearingToLocationRadian(self.yourLocation) ?? 0 }
    var yourLocation: CLLocation {
        get { return UserDefaults.standard.currentLocation }
        set { UserDefaults.standard.currentLocation = newValue }
    }
    
    
    static let shared: CompaseHeadManager = {
       return CompaseHeadManager()
    }()
    
    
    private override init() {
        super.init()
        
        cmMotionManager.magnetometerUpdateInterval = 1/100 // 100 Hrz
    }
    
     #if os(iOS)
    private func configureLocationManager() {
        
        if (CLLocationManager.headingAvailable()) {
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.headingFilter = 1
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingHeading()
            locationManager.delegate = self
        }
    }
    
    private func orientationAdjustment() -> CGFloat {
        
        let isFaceDown: Bool = {
            switch UIDevice.current.orientation {
            case .faceDown: return true
            default: return false
            }
        }()
        
        
        let adjAngle: CGFloat = {
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:  return 90
            case .landscapeRight: return -90
            case .portrait, .unknown: return 0
            case .portraitUpsideDown: return isFaceDown ? 180 : -180
            }
        }()
        return adjAngle
    }
    #endif
    
   private func getAtan2(y: Double, x: Double) -> Double {
        return atan2(y, x);
    }
    
    private func findDirection(endpoint: CLLocation, startpoint: CLLocation) -> String {
     
        let x1 = endpoint.coordinate.latitude
        let y1 = endpoint.coordinate.longitude
        let x2 = startpoint.coordinate.latitude
        let y2 = startpoint.coordinate.longitude
        
        let radians = self.getAtan2(y: (y1 - y2), x: (x1 - x2))
        let compassReading = radians * (180 / .pi)
        var coordNames: [String] = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"]
        var coordIndex = round(compassReading / 45)
        if (coordIndex < 0) {
            coordIndex = coordIndex + 8
        }
        
        let index = Int(coordIndex)
        if index >= 0 && index < coordNames.count {
            
            return coordNames[index]
        } else {
            return coordNames.first!
        }
    }
}

extension CompaseHeadManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else { return }
        
        if locationCallback != nil {
            
            locationCallback?(currentLocation)
        }
        
        if watchLocationCallback != nil {
            
            watchLocationCallback?(currentLocation)
        }
    }
    
    #if os(iOS)
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        if headingCallback != nil {
            headingCallback?(newHeading.trueHeading)
        }
    }
    #endif
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("⚠️ Error while updating location " + error.localizedDescription)
    }
}


extension CompaseHeadManager {
    
    //MARK: Public:
    
    #if os(iOS)
    public func getHeadDirectionAngel(onRunBlock:@escaping (_ newAngle: CGFloat, _ headAngle: String) -> Void) -> Void {
        
        self.configureLocationManager()
        
        self.headingCallback = { newHeading in
            
            func computeNewAngle(with newAngle: CGFloat) -> CGFloat {
            
                let heading: CGFloat = {
                    let originalHeading = self.yourLocationBearing - newAngle.degreesToRadians
                    switch UIDevice.current.orientation {
                    case .faceDown: return -originalHeading
                    default: return originalHeading
                    }
                }()
                
                return CGFloat(self.orientationAdjustment().degreesToRadians + heading)
            }
            
            let headAngle = String.init(format: "%.0f",ceil(newHeading))
            let angle = computeNewAngle(with: CGFloat(newHeading))
            let roundangle = ceil(angle)

            onRunBlock(roundangle, headAngle)
        }
        
        self.locationCallback = { location in
            
            self.latestLocation = location
        }
    }
    
    public func stopHeadDirectionDetection() {
        
        self.locationManager.stopUpdatingHeading()
        self.headingCallback = nil
        self.locationCallback = nil
    }
    
    public func getHeadDirection(onRunBlock: @escaping (_ name: String)-> Void) -> Void {
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        
        self.configureLocationManager()
        let endpointLat = 90.0000;
        let endpointLng = 0.0000
    
        let sourcePoint : CLLocation = CLLocation(latitude: endpointLat, longitude: endpointLng)
        
        
        self.watchLocationCallback = { location  in
            
            let destination : CLLocation = location
         let head = self.findDirection(endpoint: destination, startpoint: sourcePoint)
            
            onRunBlock(head)
        }
    }
    
    #endif
    public func getHeadAngle(onRunBlock: @escaping (_ name: CGFloat)-> Void) -> Void {
        
        
        if  cmMotionManager.isMagnetometerAvailable {
            
            cmMotionManager.startMagnetometerUpdates(to: OperationQueue.main) { (magnetometerData, error) in
                
                if error == nil && magnetometerData != nil {
                    
                    var heading: Double = 0.0
                    let x = magnetometerData?.magneticField.x
                    let y = magnetometerData?.magneticField.y
                    
                    let zero: Double = 0.0
                    
                    if y! > 0 {
                        heading = 90.0 - atan(x!/y!)*180.0/Double.pi;
                        
                    }
                    if y! < 0  {
                        heading = 270.0 - atan(x!/y!)*180.0/Double.pi
                    }
                    if y! == zero && x! < 0 {
                        
                        heading = 180.0
                        
                    }
                    if y! == zero && x! > 0 {
                        
                        heading = 0
                    }
                    
                    let ceilAngle = ceil(heading)
                    onRunBlock(CGFloat(ceilAngle))
                    print("Heading Angle = ",heading)
                }
            }
            
        }  else {
            
            onRunBlock(CGFloat(0))
            print("Megnotometer not available")
        }
    }
    
    public func stopHeadAngle() {
        cmMotionManager.stopMagnetometerUpdates()
    }

}
