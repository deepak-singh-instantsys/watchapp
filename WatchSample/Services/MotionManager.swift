//
//  MotionManager.swift
//  Sensors
//
//  Created by Deepak on 11/8/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreMotion

class MotionManager: NSObject {
    
    var cmManager:CMMotionManager?
    
    override init() {
        super.init()
        self.initialize()
    }
    
    deinit {
    }
}

extension MotionManager {
    
    
    public func initialize() {
        
        if self.cmManager == nil {
            
            self.cmManager = CMMotionManager()
            self.cmManager?.accelerometerUpdateInterval = 1/30    //0.4 Hz
            self.cmManager?.deviceMotionUpdateInterval = 1/30   //0.4 Hz
            self.cmManager?.gyroUpdateInterval = 1/30           //0.4 Hz
        }
    }
    
    
    public func startGyroscope(onRun:@escaping (_ data: CMGyroData?, _ error: Error?, _ errorMessage: String?)-> Void) {
        
        if (self.cmManager?.isGyroAvailable)! {
            
            let handler:CMGyroHandler = {(data: CMGyroData?, error: Error?) -> Void in
                
                if error != nil {
                    
                    //self.stopGyroscopeUpdate()
                }
                
                onRun(data, error, nil)
                
            }
    
            self.cmManager?.startGyroUpdates(to: OperationQueue.main, withHandler: handler)
        }
            
        else {
            
            let error  = NSError(domain: "Apple watch/iPhone sensors", code: 100, userInfo: [NSLocalizedDescriptionKey : "Gyro Not available"])
             let msg = "Not supported"
           // self.stopGyroscopeUpdate()
            onRun(nil , error, msg)
        }
    }
    
    public func getAccelerometerData(onRun:@escaping (_ accelerometerData: CMAccelerometerData?, _ error: Error?, _ errorMessage: String?)-> Void) {
        
        if (cmManager?.isAccelerometerAvailable)! {
            
            self.cmManager?.startAccelerometerUpdates(to: OperationQueue.main, withHandler: { (accelerometerData, error) in
                
                if accelerometerData != nil && error == nil {
                    
                    onRun(accelerometerData, error, nil)
                }
            })
        }
        else {
            
            let error  = NSError(domain: "Apple watch/iPhone sensors", code: 100, userInfo: [NSLocalizedDescriptionKey : "Accelerometer not available"])
            let msg = "Accelerometer not available"
            self.stopMotionManager()
            onRun(nil, error, msg)
        }
    }
    
    
    public func getDeviceMotionData(onRun:@escaping (_ deviceMotion: CMDeviceMotion?, _ error: Error?, _ errorMessage: String?)-> Void) {
    
        if (cmManager?.isAccelerometerAvailable)! {
            cmManager?.startDeviceMotionUpdates(to: OperationQueue.main, withHandler: { (deviceMotion, error) in
                
                if error == nil {
                   
                    onRun(deviceMotion, error, nil)
                   
                } else {
                    
                    self.stopMotionManager()
                }
            })
        }
        else {
            
            let error  = NSError(domain: "Apple watch/iPhone sensors", code: 100, userInfo: [NSLocalizedDescriptionKey : "Accelerometer not available"])
            let msg = "Accelerometer not available"
             self.stopMotionManager()
            onRun(nil, error, msg)
        }
    }
    
    public func stopMotionManager() -> Void {

        DispatchQueue.main.async {
            
            self.cmManager?.stopDeviceMotionUpdates()
            self.cmManager?.stopAccelerometerUpdates()
            self.cmManager?.stopGyroUpdates()
            self.cmManager?.stopMagnetometerUpdates()
            self.cmManager = nil
        }
    }
    
    
    
    public func getHeading(onRun:@escaping (_ cmMotion: CMDeviceMotion?, _ error: Error?, _ errorMessage: String?)-> Void) {
        
        let mainQueue = OperationQueue.main
       // cmManager.showsDeviceMovementDisplay = true
    
        cmManager?.startDeviceMotionUpdates(using: .xMagneticNorthZVertical, to: mainQueue) { (deviceMotion, error) in
            
            if deviceMotion != nil && error == nil {
                
            onRun(deviceMotion,nil,nil)
               
            } else {
                
                 onRun(nil,nil,nil)
            }
        }
    }
}




