//
//  AltimeterManager.swift
//  Sensors
//
//  Created by Deepak on 11/8/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreMotion

class AltimeterManager: NSObject {

    var altimeter = CMAltimeter()
    static let shared: AltimeterManager = {
        return AltimeterManager()
    }()
    
    private override init() {
        
        super.init()
    }
}


extension AltimeterManager {
    
    //MARK: Public
    
    public func getAltimeterData(onRun:@escaping (_ altitudeData: CMAltitudeData?, _ error: Error?, _ errorMessage: String?)-> Void) {
        
         if (CMAltimeter.isRelativeAltitudeAvailable()) {
            
            self.altimeter.startRelativeAltitudeUpdates(to: OperationQueue.main, withHandler: { (data, error) in
                
                if error != nil {
                    
                    self.stopAltimeter()
                }
                onRun(data, error, nil)
            })
            
         } else {
            
            self.stopAltimeter()
            let error  = NSError(domain: "Apple watch/iPhone sensors", code: 100, userInfo: [NSLocalizedDescriptionKey : "Barometer not available on this device."])
            let msg = "Not supported"
            onRun(nil, error, msg)
        }
    }
    
    public func stopAltimeter() {
        
        self.altimeter.stopRelativeAltitudeUpdates()
    }
}
