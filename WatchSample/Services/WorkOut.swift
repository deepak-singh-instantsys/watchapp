//
//  WorkOut.swift
//  WatchSample
//
//  Created by Deepak on 28/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import HealthKit

class WorkOut: NSObject {

    
    var processingSampleBlock:((_ samples: [HKSample]?, _ quantityTypeIdentifier: HKQuantityTypeIdentifier) -> Void)?
    var session: HKWorkoutSession?
    var heartRateQuery : HKQuery?
    var workoutStartDate = Date()
    
    //HKQuantity *distanceTravelled;
    var currentQueries = [HKQuery]()
    private let healthStore = HKHealthStore()
    
    
    override init() {
        
        super.init()
    }
    
    
    private  func startWorkout() {
        
        if (session != nil) {
            return
        }
        
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = .mixedCardio
        workoutConfiguration.locationType = .outdoor
        
        do {
            session = try HKWorkoutSession(configuration: workoutConfiguration)
            session?.delegate = self
        } catch {
            
            fatalError("Unable to create the workout session!")
        }
        
        healthStore.start(self.session!)
    }
}

extension WorkOut {
    
    //MARK: Public
    
    public func startWorout(onRun: @escaping (_ samples: [HKSample]?, _ quantityTypeIdentifier: HKQuantityTypeIdentifier) -> Void) {
        self.processingSampleBlock = onRun
        self.startWorkout()
    }
    
    public func stopWorkout() {
        
        if self.session != nil {
            
            healthStore.end(self.session!)
        }
        
    }
    
    func getStepsFromDate(fromDate: Date, completion: @escaping (Double) -> Void) {
        
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        
        let startOfDay = Calendar.current.startOfDay(for: fromDate)
        
        let now = Date()
        
        let predicate = HKQuery.predicateForSamples(withStart: fromDate, end: now, options: .strictStartDate)
        
        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                print("\(String(describing: error?.localizedDescription)) ")
                completion(resultCount)
                return
            }
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.count())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthStore.execute(query)
    }
}

extension WorkOut: HKWorkoutSessionDelegate {
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        
        self.stopWorkout()
    }
    
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        
        switch toState {
        case .running:
            startDataCollection()
        case .ended:
            workoutDidEnd()
        default:
            print("Unexpected state \(toState)")
        }
    }
    
    func startDataCollection() -> Void {
        
        self.startDataQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier.heartRate)
        self.startDataQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)
        self.startDataQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier.stepCount)
        self.startDataQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier.flightsClimbed)
    }
    
    
    
    func startDataQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: quantityTypeIdentifier) else { return
     }
        
        let datePredicate = HKQuery.predicateForSamples(withStart: self.workoutStartDate, end: nil, options: .strictStartDate)
        let set: Set<HKDevice> = NSSet(objects: HKDevice.local()) as! Set<HKDevice>
        let devicePredicate = HKQuery.predicateForObjects(from: set)
        
        let queryPredicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [datePredicate, devicePredicate])
      
        
        let query = HKAnchoredObjectQuery(type: quantityType, predicate: queryPredicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            self.processSamples(samples: sampleObjects, quantityTypeIdentifier: quantityTypeIdentifier)
        }
        
        query.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
            
            self.processSamples(samples: samples, quantityTypeIdentifier: quantityTypeIdentifier)

        }
        
        healthStore.execute(query)
        self.currentQueries.append(query)
    }
    
    private  func processSamples(samples: [HKSample]?, quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        
        if self.processingSampleBlock != nil {
            
            self.processingSampleBlock!(samples, quantityTypeIdentifier)
        }
    }
}

extension WorkOut {
    //MARK: Private
    
    
    private  func workoutDidStart(_ date : Date) {
        
        if let heartRateQuery = createHeartRateStreamingQuery(date) {
            healthStore.execute(heartRateQuery)
        }
    }
    
    private  func workoutDidEnd() {
        
        for query in currentQueries {
            
            if query != nil {
                 healthStore.stop(query)
            }
        }
        session = nil
    }
    

    private func createHeartRateStreamingQuery(_ workoutStartDate: Date) -> HKQuery? {
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return nil }
        let datePredicate = HKQuery.predicateForSamples(withStart: workoutStartDate, end: nil, options: .strictEndDate )
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates:[datePredicate])
        
        
        let heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            self.updateHeartRate(sampleObjects)
        }
        
        heartRateQuery.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
            
            self.updateHeartRate(samples)
        }
        return heartRateQuery
    }
    
    private  func updateHeartRate(_ samples: [HKSample]?) {
        
        guard let heartRateSamples = samples as? [HKQuantitySample] else {
            
            print("Failed to get heart rate")
            return
        }
        
        DispatchQueue.main.async {
            
            let heartRateUnit = HKUnit(from: "count/min")
            guard let sample = heartRateSamples.first else{return}
            let value = sample.quantity.doubleValue(for: heartRateUnit)
            print("Heart Rate =\(value)")
        }
    }
    
    
    
    private func createTotalDistanceStreamingQuery(_ workoutStartDate: Date) -> HKQuery? {
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return nil }
        let datePredicate = HKQuery.predicateForSamples(withStart: workoutStartDate, end: nil, options: .strictEndDate )
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates:[datePredicate])
        
        
        let heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            self.updateHeartRate(sampleObjects)
        }
        
        heartRateQuery.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
            
            self.updateHeartRate(samples)
        }
        return heartRateQuery
    }
    
    private  func updateSteps(_ samples: [HKSample]?) {
        
        guard let heartRateSamples = samples as? [HKQuantitySample] else {
            
            print("Failed to get heart rate")
            return
        }
        
        DispatchQueue.main.async {
            
            let heartRateUnit = HKUnit(from: "count/min")
            guard let sample = heartRateSamples.first else{return}
            let value = sample.quantity.doubleValue(for: heartRateUnit)
            print("Heart Rate =\(value)")
        }
    }
}
