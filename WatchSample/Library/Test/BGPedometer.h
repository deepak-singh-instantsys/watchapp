//
//  NSObject+Test.h
//  WatchSample
//
//  Created by Deepak on 25/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BGPedometer : NSObject {
    
}

- (void)getFromDate:(NSDate *)fromDate onRun:(void (^)(id, int))iteratorBlock;
@end
