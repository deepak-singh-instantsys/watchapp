//
//  NSObject+Test.m
//  WatchSample
//
//  Created by Deepak on 25/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//


#import "BGPedometer.h"
#import <HealthKit/HealthKit.h>

@implementation BGPedometer

- (void)getFromDate:(NSDate *)fromDate onRun:(void (^)(id, int))iteratorBlock {
    
    HKHealthStore *healthStore = [[HKHealthStore alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *interval = [[NSDateComponents alloc] init];
    interval.second = 1;
    
    NSDateComponents *anchorComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                     fromDate:fromDate];
    anchorComponents.hour = 0;
    NSDate *anchorDate = [calendar dateFromComponents:anchorComponents];
    HKQuantityType *quantityType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    
    // Create the query
    HKStatisticsCollectionQuery *query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:quantityType
                                                                           quantitySamplePredicate:nil
                                                                                           options:HKStatisticsOptionCumulativeSum
                                                                                        anchorDate:anchorDate
                                                                                intervalComponents:interval];
    
    // Set the results handler
    query.initialResultsHandler = ^(HKStatisticsCollectionQuery *query, HKStatisticsCollection *results, NSError *error) {
        if (error) {
            // Perform proper error handling here
            NSLog(@"*** An error occurred while calculating the statistics: %@ ***",error.localizedDescription);
        }
        
      
        NSDate *now = [[NSDate alloc] init];
        // Plot the daily step counts over the past 7 days
        [results enumerateStatisticsFromDate:fromDate
                                      toDate:now
                                   withBlock:^(HKStatistics *result, BOOL *stop) {
                                       
                                       HKQuantity *quantity = result.sumQuantity;
                                       if (quantity) {
                                           NSDate *date = result.startDate;
                                           double value = [quantity doubleValueForUnit:[HKUnit countUnit]];
                                           NSLog(@"%@: %f", date, value);
                                       }
                                       
                                   }];
    };
    
    [healthStore executeQuery:query];
}
@end
