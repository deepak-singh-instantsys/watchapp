//
//  AppDelegate.swift
//  WatchSample
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData

import WatchConnectivity



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabbarController: UITabBarController?
    var backgroundTask = UIBackgroundTaskInvalid
    var allDataViewController: AllDataViewController?
    var session: WCSession?
    var arrData = [Dictionary<String,Any>]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.showHomeViewC()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            
            self.setupWCSession()
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        print("applicationDidEnterBackground")
        backgroundTask = application.beginBackgroundTask(expirationHandler: {
            //BackGround Task Finished
            print("BackGround Task Finished")
            application.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = UIBackgroundTaskInvalid
        })
        
        let priority = DispatchQoS.background
        let backgroundQueue = DispatchQueue.global()
        let deadline = DispatchTime.now() + .seconds(0)
        backgroundQueue.asyncAfter(deadline: deadline, qos: priority) {
            
            // Begin your upload
            
            
            
            
            
            
            print("Begin your upload")
            application.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = UIBackgroundTaskInvalid
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        //CoreDataManager.saveContext(CoreDataManager.sharedInstance.privateQueueCtxt)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // CoreDataManager.saveContext(CoreDataManager.sharedInstance.privateQueueCtxt)
    }

    func applicationWillTerminate(_ application: UIApplication) {
      
        //CoreDataManager.saveContext(CoreDataManager.sharedInstance.privateQueueCtxt)

    }
    
    func application(_ application: UIApplication, handleWatchKitExtensionRequest userInfo: [AnyHashable : Any]?, reply: @escaping ([AnyHashable : Any]?) -> Void) {
        
        print("handleWatchKitExtensionRequest")
        
        
        
        backgroundTask = application.beginBackgroundTask(expirationHandler: {
            //BackGround Task Finished
            print("BackGround Task Finished")
           
            // This expirationHandler is called when your task expired
            // Cleanup the task here, remove objects from memory, etc
            
            application.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = UIBackgroundTaskInvalid
        })
        
        let priority = DispatchQoS.background
        let backgroundQueue = DispatchQueue.global()
        let deadline = DispatchTime.now() + .seconds(0)
        backgroundQueue.asyncAfter(deadline: deadline, qos: priority) {
            
            // Begin your upload
            
             print("Begin your upload")
            application.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = UIBackgroundTaskInvalid
        }
    }
}

extension AppDelegate {
    
    //MARK: Private
    
    private func setupWCSession() {
        
        if WCSession.isSupported() {
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        }
    }
    
    private func showViewController() {
        
        /*let isLogin = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KLogedIn))
        if isLogin == "1" {
            self.showHomeViewC()
        } else {
            self.showLogin()
        }*/

    }
}

extension AppDelegate {
    //MARK: Public
    
    public func showHomeViewC() {
        
        let sto = UIStoryboard(name: "Main", bundle: nil)
        let viewC = sto.instantiateViewController(withIdentifier: "ICloudeDesboardViewController") as? ICloudeDesboardViewController
        self.window?.rootViewController = viewC
    }

    
    public func showLogin() {
        
        let sto = UIStoryboard(name: "Main", bundle: nil)
        let login = sto.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.window?.rootViewController = login
    }
    
    public func sendDictInfo(dictInfo:Dictionary<String, Any>) {
        
        self.session?.sendMessage(dictInfo, replyHandler: { (dictReply) in
            print("Replay=",dictReply)
        }, errorHandler: { (error) in
            print(error.localizedDescription)
        })
    }
}

extension AppDelegate: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("didReceiveMessage")
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        
        let isLogin = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KLogedIn))
        
        if let type = applicationContext[KMESSAGE_TYPE] as? String {
            if type == KMESSAGE_TYPE_AUTHACK && isLogin == "1" {
                
                KUserDefault.setValue("1", forKey: KWATCH_AUTH)
                KUserDefault.synchronize()
                self.allDataViewController?.didReceivAuthAck()
                
            } else if type == KMESSAGE_TYPE_AUTH_REQUEST && isLogin == "1" {
                
                let userName = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KUSERNAME))
                let password = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KPASSWORD))
                let dictAuth = [KMESSAGE_TYPE : KMESSAGE_TYPE_AUTH, KUSERNAME : userName, KPASSWORD: password]
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self.sendDictInfo(dictInfo: dictAuth)
                })
            } else if type == KMESSAGE_TYPE_SENSOR_OBSERVATION {
                
                self.allDataViewController?.didSensorObservationReceived(dictInfo: applicationContext)
            }
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        let isLogin = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KLogedIn))
        
        if let type = message[KMESSAGE_TYPE] as? String {
            if type == KMESSAGE_TYPE_AUTHACK && isLogin == "1" {
                
                KUserDefault.setValue("1", forKey: KWATCH_AUTH)
                KUserDefault.synchronize()
                self.allDataViewController?.didReceivAuthAck()
                
            } else if type == KMESSAGE_TYPE_AUTH_REQUEST && isLogin == "1" {
                
                let userName = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KUSERNAME))
                let password = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KPASSWORD))
                let dictAuth = [KMESSAGE_TYPE : KMESSAGE_TYPE_AUTH, KUSERNAME : userName, KPASSWORD: password]
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self.sendDictInfo(dictInfo: dictAuth)
                })
            } else if type == KMESSAGE_TYPE_SENSOR_OBSERVATION {
                
                self.allDataViewController?.didSensorObservationReceived(dictInfo: message)
            }
        }
    }
}

