//
//  ServerDataProcessor.swift
//  WatchApp Extension
//
//  Created by Deepak on 07/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit

protocol ServerDataProcessorEvent: class {
    
    func didDataUploadFinishEvent(dictData: Dictionary<String, Any>) -> Void
    func didDataUploadFailedEvent(dictData: Dictionary<String, Any>) -> Void
    func didBatchUploadComplete() -> Void
}


protocol ServerDataProcessorProtocol: class {
    
    //ServerDataProcessor -->> DataProcessor
    weak var delegate: ServerDataProcessorEvent? {set get}
    func uploadArrayOfDataToServer(arrData: [Dictionary<String, Any>]) -> Void
    
}


class ServerDataProcessor: NSObject, ServerDataProcessorProtocol {
    
    var delegate: ServerDataProcessorEvent?
    var arrUploadingDataInProcess = [Dictionary<String, Any>]()
    var uploadCounter = 0

    func uploadArrayOfDataToServer(arrData: [Dictionary<String, Any>]) {
        
        arrUploadingDataInProcess = arrData
        print("ArrUploadingDataInProcess=",self.arrUploadingDataInProcess.count)
        if arrUploadingDataInProcess.count > 0 {

            
        } else {
            
            self.delegate?.didBatchUploadComplete()
            self.uploadCounter = 0
        }
    }
    
    func uploadData(dictData: Dictionary<String, Any>, onCompletion:@escaping (_ dictData: Dictionary<String, Any>, _ sucess: Bool)-> Void) -> Void {
        
        if let data = dictData[KDATA] as? Data {
            
            APIClient.shared.uploadDataToServer(data: data, onCompletion: { (success) in
                if success {
                    onCompletion(dictData, true)
                    
                } else {
                    onCompletion(dictData, false)
                }
            })
        }
    }
}
