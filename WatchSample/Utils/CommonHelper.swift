//
//  CommonHelper.swift
//  Sensors
//
//  Created by Deepak on 11/8/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit

class CommonHelper: NSObject {

    class func convertToString(obj : Any?) -> String
    {
        
        if obj == nil
        {
            return ""
        }
        
        let strBlank = ""
        
        if let _ = obj as? String
        {
            let aString = self.validateString(aString: obj as? String)
            return aString
        }
        else
        {
            if obj is String == true || obj is NSNumber == true || obj is Bool == true ||  obj is Int == true
                
            {
                let aString = String(describing: obj!)
                return aString
            }
        }
        
        return strBlank
    }
    
    class func toString(object:Any?) -> String
    {
        if let str = object as? String
        {
            return String(format: "%@", str)
        }
        
        if let num = object as? NSNumber
        {
            return String(format: "%@", num)
        }
        
        return ""
    }
    
    
    class func validateString(aString : String?) -> String
    {
        let strBlank = ""
        
        if aString == nil
        {
            return strBlank
        }
        
        if let _ = aString
        {
            if (CommonHelper.isStringValid(string: aString!) && CommonHelper.isStringValid(string: aString!))
            {
                return aString!
            }
        }
        
        return strBlank
    }
    
    class func isStringValid(string: String?) -> Bool
    {
        
        var str: String?;
        
        if string != nil
        {
            str = string!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        else         {
            return false;
        }
        
        if str!.isEmpty
        {
            return false;
        }
        return true;
    }
    
    
    class  func getCurrentDate() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyy HH:mm:ss a"
        formatter.dateStyle = .long
        let today = Date.init()
        var strDate = formatter.string(from: today)
        
        let timeFormater  = DateFormatter()
        timeFormater.dateFormat = "HH:mm:ss a"
        let strTime = timeFormater.string(from: today)

        strDate = strDate + " " + strTime
        return strDate
    }
    
    
    class  func getCurrentTimeStamp() -> Double {
    
        let today = Date.init()
        let timeInterval = today.timeIntervalSince1970
        return timeInterval
    }
    
    class  func getTimeStamp(date: Date) -> Double {
        
        let timeInterval = date.timeIntervalSince1970
        return timeInterval
    }
    
    
    class  func getSessionId() -> Int64 {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let today = Date.init()
        let strDate = formatter.string(from: today)
        let date = formatter.date(from: strDate)
        let sessionId = date?.timeIntervalSince1970
        return Int64(sessionId!)
    }
    
    class  func getDateFromTimeStamp(timeStamp: Double) -> String {
        
        let date = Date.init(timeIntervalSince1970: timeStamp)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy h:mm:ss a"
        let strDate = formatter.string(from: date)
        return strDate
    }
    
    class  func getTodayDateFromTimeStamp(timeStamp: Double) -> String {
        
        let date = Date.init(timeIntervalSince1970: timeStamp)
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        let strDate = formatter.string(from: date)
        return strDate
    }
    
    class func isDictionaryEqual(dict1: Dictionary<String, Any>, dict2: Dictionary<String, Any>) -> Bool {
     
        let keys1 = dict1.keys
        let keys2 = dict2.keys
        
        if keys1.count != keys2.count {
            return false
        }
        
        
        for key in keys1 {
            
            guard let value1 = dict1[key] as? String, let value2 = dict2[key] as? String, value1 == value2 else {
                
                return false
            }
        }
        return true
    }
}
