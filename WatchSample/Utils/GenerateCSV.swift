//
//  GenerateCSV.swift
//  WatchSample
//
//  Created by Deepak on 11/16/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData
import MessageUI



class GenerateCSV: NSObject {

    var viewController: UIViewController?
    
    let operationQueue = OperationQueue()
    
    static let shared: GenerateCSV = {
       
        return GenerateCSV()
    }()

    func generateAllDataCSV(fromViewC: UIViewController) -> Void {
        
        self.viewController = fromViewC
        
        let arrRecords = getAllData()
        if arrRecords == nil {
            return
        }
        
        self.writeCoreDataObjectToCSVForAllData(objects: arrRecords!) { (csvString) in
            
            let data = csvString.data(using: String.Encoding.utf8)
            
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            if data != nil {
                self.sendEmail(data: data!, subject: "Activity")
            }
        }
    }
}

extension GenerateCSV {
    
    //MARK: Private
    
    private func sendEmail(data: Data, subject: String) {
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["deepak.singh@instantsys.com"])
        composeVC.setSubject(subject)
        composeVC.setMessageBody("Data", isHTML: false)
        composeVC.addAttachmentData(data, mimeType: "text/csv", fileName: subject)
        
        DispatchQueue.main.async {
            
            self.viewController?.present(composeVC, animated: true, completion: nil)

        }
    }
    
    private func writeCoreDataObjectToCSVForAllData(objects: [AllData], onCompleted:@escaping (_ export: String)-> Void) {
        
        /*var export: String = "timeStamp,xUser_acceleration,yUser_acceleration, zUser_acceleration,xRotationRate, yRotationRate, zRotationRate,xQuaternion,yQuaternion,zQuaternion,wQuaternion, Activity-Name,Heart-Rate, Steps,Distance, Cadence, Pace,Flight-Asc,Flight-dsc,Rotation Angle, Heading Angle, Latitude, Longitude,  xAccelAccelerometer, yAccelAccelerometer, zAccelAccelerometer, Surrounding Tempreture in Celsius ,Altitude, Pressure \n"
        
        let serialQueue = OperationQueue()
        let group = DispatchGroup()
        serialQueue.maxConcurrentOperationCount = 1
        
        group.enter()
        for (index, data) in objects.enumerated() {
            
            group.enter()
            let blockOp = BlockOperation()
            
            blockOp.addExecutionBlock {
                
                if index < objects.count - 1 {
                    
                    let time = CommonHelper.convertToString(obj: data.timeStamp)
                    let xAcceleration = CommonHelper.convertToString(obj: data.xUser_acceleration)
                    let yAcceleration = CommonHelper.convertToString(obj: data.yUser_acceleration)
                    let zAcceleration = CommonHelper.convertToString(obj: data.zUser_acceleration)
                    let xRotation = CommonHelper.convertToString(obj: data.xRotationRate)
                    let yRotation = CommonHelper.convertToString(obj: data.yRotationRate)
                    let zRotation = CommonHelper.convertToString(obj: data.zRotationRate)
                    let xQuaternion = CommonHelper.convertToString(obj: data.xQuaternion)
                    let yQuaternion = CommonHelper.convertToString(obj: data.yQuaternion)
                    let zQuaternion = CommonHelper.convertToString(obj: data.zQuaternion)
                    let wQuaternion = CommonHelper.convertToString(obj: data.wQuaternion)
                    let activity = CommonHelper.convertToString(obj: data.activity)
                    let heartRate = CommonHelper.convertToString(obj: data.heartRate)
                    let steps = CommonHelper.convertToString(obj: data.steps)
                    let distance = CommonHelper.convertToString(obj: data.distance)
                    let cadence = CommonHelper.convertToString(obj: data.cadence)
                    let pace = CommonHelper.convertToString(obj: data.pace)
                    let flightAsc = CommonHelper.convertToString(obj: data.flightAsc)
                    let flightdsc = CommonHelper.convertToString(obj: data.flightdsc)
                    let rotationAngle = CommonHelper.convertToString(obj: data.rotationAngle)
                    let headingAngle = CommonHelper.convertToString(obj: data.headingAngle)
                    let latitude = CommonHelper.convertToString(obj: data.latitude)
                    let longitude = CommonHelper.convertToString(obj: data.longitude)
                    let xAccelAccelerometer = CommonHelper.convertToString(obj: data.xAccelAccelerometer)
                    let yAccelAccelerometer = CommonHelper.convertToString(obj: data.yAccelAccelerometer)
                    let zAccelAccelerometer = CommonHelper.convertToString(obj: data.zAccelAccelerometer)
                    let surroundingTemp = CommonHelper.convertToString(obj: data.surroundingTempreture)
                    let altitude = CommonHelper.convertToString(obj: data.altitude)
                    let presure = CommonHelper.convertToString(obj: data.presure)
            
                    let newStr = "\(time),\(xAcceleration),\(yAcceleration),\(zAcceleration),\(xRotation),\(yRotation),\(zRotation),\(xQuaternion),\(yQuaternion),\(zQuaternion),\(wQuaternion),\(activity),\(heartRate),\(steps),\(distance),\(cadence),\(pace),\(flightAsc),\(flightdsc), \(rotationAngle), \(headingAngle), \(latitude), \(longitude), \(xAccelAccelerometer), \(yAccelAccelerometer), \(zAccelAccelerometer) , \(surroundingTemp), \(altitude), \(presure)\n"
                    export = export + newStr
                }
                group.leave()
            }
            serialQueue.addOperation(blockOp)
        }
        
        group.leave()
       
        group.notify(queue: DispatchQueue.main) {
            onCompleted(export)
        }*/
    }
    
    
    private func getAllData() -> [AllData]?  {
        
        let moc = CoreDataManager.sharedInstance.privateQueueCtxt
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AllData")
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            
            let arr = try moc?.fetch(fetchRequest) as! [AllData]
            if arr.count > 0 {
                
                return arr
            }
            
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
        
        return nil
    }
}

extension GenerateCSV: MFMailComposeViewControllerDelegate {
    
     public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)

    }
    
}
