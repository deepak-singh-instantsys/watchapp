//
//  CoreDataManager.swift
//  Sensors
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData

let KDataBase = "WatchSample"
let KAppGroup = "group.instantSys.Sensors"

class CoreDataManager: NSObject {
    
    public static let sharedInstance = CoreDataManager()
    
    override init() {
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidSavePrivateQueueContext(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: self.privateQueueCtxt)
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidSaveMainQueueContext(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: self.mainQueueCtxt)
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func contextDidSavePrivateQueueContext(_ notification: Notification) {
        
        if let context = self.mainQueueCtxt {
            self.synced(self, closure: { () -> () in
                context.perform({() -> Void in
                    context.mergeChanges(fromContextDidSave: notification)
                })
            })
        }
    }
    
    @objc func contextDidSaveMainQueueContext(_ notification: Notification) {
        
        if let context = self.privateQueueCtxt {
            self.synced(self, closure: { () -> () in
                context.perform({() -> Void in
                    context.mergeChanges(fromContextDidSave: notification)
                })
            })
        }
    }
    
    func synced(_ lock: AnyObject, closure: () -> ()) {
        
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
    
    lazy var applicationDocumentsDirectory: URL = {
        
        let urls = Foundation.FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        

        let modelURL = Bundle.main.url(forResource: KDataBase, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        
        
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let options = [
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true
        ]
        let directory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: KAppGroup)!
        
        let url = directory.appendingPathComponent(KDataBase + ".sqlite")
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch var error as NSError {
            coordinator = nil
            NSLog("Unresolved error \(error), \(error.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        return coordinator
    }()
    
    lazy var mainQueueCtxt: NSManagedObjectContext? = {
       
        var managedObjectContext = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    lazy var privateQueueCtxt: NSManagedObjectContext? = {
        
        var managedObjectContext = NSManagedObjectContext(concurrencyType:.privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
}

extension CoreDataManager {
    
    //MARK: Public

    public func start() {
        
    }
    
    
    public class func privateQueueContext() -> NSManagedObjectContext {
        
        return self.sharedInstance.privateQueueCtxt!
    }
    
    
    open class func mainQueueContext() -> NSManagedObjectContext {
        
        return self.sharedInstance.mainQueueCtxt!
    }
    
    
    public class func saveContext(_ context: NSManagedObjectContext?) {
        
        context?.perform({
            
            if let moc = context {
                if moc.hasChanges {
                    do {
                        try moc.save()
                    } catch {
                    }
                }
            }
        })
    }
}

extension NSManagedObject {
    
    public class func findAllForEntity(_ entityName: String, context: NSManagedObjectContext) -> [AnyObject]? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let result: [AnyObject]?
        do {
            result = try context.fetch(request)
        } catch let error as NSError {
            print(error)
            result = nil
        }
        return result
    }
    
}
