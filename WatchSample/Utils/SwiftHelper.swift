//
//  SwiftHelper.swift
//  Answer
//
//  Created by Deepak Singh on 13/02/17.
//  Copyright © 2017 Prodge. All rights reserved.
//

import UIKit
import SystemConfiguration


class SwiftHelper: NSObject
{
    
   
    
    class func isIPAD()->Bool
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            return true
        }
        return false
    }
    
    class func isIPHONE()->Bool
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            return true
        }
        return false
    }

    class func getViewControllerFromStoryBoard(stoName:String , identifier:String)-> UIViewController
    {
        let storyBoard = UIStoryboard(name: stoName, bundle: nil);
        let viewController:UIViewController = storyBoard.instantiateViewController(withIdentifier: identifier);
        return viewController;
    }
    
    class func delayInMainQueue(delay:Double, closure:(()->())?)
    {
        
    }
    class func getMainQueue(closure:(()->())?) {
        DispatchQueue.main.async {
            
        }
    }
    
    class func isHeightGreater568()-> Bool
    {
        if UIScreen.main.bounds.size.height > 568
        {
            return true
        }
        return false
    }
    
    class func  getColor(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) -> UIColor
    {
        let color =  UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alpha)
        return color
    }
    
   
    class func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false
        {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
    
    
    class func isValidEmail(candidate: String) -> Bool
    {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        print(valid)
        return valid
    }
    
    class func isValidPhone(phone: String) -> Bool
    {
        
        let phoneRegex = "^((\\+)|(00))[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        print(valid)
        return valid
    }

    
    class func showLoaderWithInteraction(message: String?)
    {
        DispatchQueue.main.async {
            
//            SVProgressHUD.setDefaultStyle(.light)
//            SVProgressHUD.setDefaultMaskType(.none)
//            SVProgressHUD.show()
        }
    }
    
    
    class func showLoader(message: String?)
    {
        DispatchQueue.main.async {
            
//            SVProgressHUD.setDefaultMaskType(.gradient)
//            SVProgressHUD.setDefaultMaskType(.clear)
//
//            SVProgressHUD.show()
        }
    }
    
    class func dismissLoader()
    {
        DispatchQueue.main.async {
            
            //SVProgressHUD.dismiss()
        }
    }
   
    
    class func showOkAlertFor(title: String, message: String, obj: UIViewController?, completion: ((_ tag: Int) -> Void)?) -> Void
    {
        if message == ""
        {
            return
        }
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.cancel, handler: { (action : UIAlertAction) in
            
            if let _ = completion
            {
                completion!(0)
            }
        }))
        
        if let _ = obj
        {
             DispatchQueue.main.async {
                
                obj!.present(alert, animated: true, completion: {})

            }
        }
        else
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            DispatchQueue.main.async {
                
                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)

            }
        }
    }
    
    class func showAlertView(title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, obj: UIViewController, completion: @escaping (_ tag: Int) -> Void) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        
        DispatchQueue.main.async {
            
            obj.present(alert, animated: true, completion: {})
            
        }
        
    }
    
    class func showAlertOnActiveController(title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, completion: @escaping (_ tag: Int) -> Void) {
        
        guard let activeVieC = topMostController()
            else
        {
            return
        }
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        
        DispatchQueue.main.async {
            
            
            
            activeVieC.present(alert, animated: true, completion: {})
            
        }
        
        
    }
    
    
    class func showAlertOnActiveControllerFromViewController(viewController: UIViewController, title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, completion: @escaping (_ tag: Int) -> Void) {
        
       
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        
        DispatchQueue.main.async {
            
            
            viewController.present(alert, animated: true, completion: {})
        }
    }
    
    
    
    class func topMostController() -> UIViewController? {
        
        let topController =  UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = topController as? UINavigationController,
            let activeViewC = navigationController.visibleViewController {
            
            return activeViewC
        }
        if let viewC = topController {
            
            return viewC
        }
        return nil
    }

    
    class func makeImageViewCircular(imageView: UIImageView)
    {
        imageView.layer.cornerRadius = imageView.frame.size.width/2
        imageView.clipsToBounds = true
        
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 5.0
    }
    
    class func makeViewCircular(view: AnyObject)
    {
        var newView = view
        newView.layer.cornerRadius = view.frame.size.width/2
        newView.layer.borderColor = UIColor.clear.cgColor
        newView.layer.borderWidth = 1.0
        
    }
    
   class func downloadedFrom(strUrl: String, completionBlock:@escaping (_ image: UIImage?)->Void)
    {
        
        
        let url = URL(string: strUrl)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else
            {
                DispatchQueue.main.async() { () -> Void in
                    
                    completionBlock(nil)
                }
                return
            }
            DispatchQueue.main.async() { () -> Void in
                completionBlock(image)
            }
            }.resume()
    }
    
   class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func getUserAgent()-> String
    {
        
        let model = UIDevice.current.localizedModel
        let osVersion = UIDevice.current.systemVersion
        let userAgent = String(format:"%@:%@",model,osVersion)
        return userAgent
    }
    
    
}
