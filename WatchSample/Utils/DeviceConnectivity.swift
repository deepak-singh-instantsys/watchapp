//
//  DeviceConnectivity.swift
//  Sensors
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import WatchConnectivity

class DeviceConnectivity: NSObject {
    
    var session: WCSession?
    var arrData = [Dictionary<String,Any>]()

    static let shared: DeviceConnectivity = {
        
        return DeviceConnectivity()
        
    }()
    
    private override init() {
        super.init()
        self.setupWCSession()
    }
}



extension DeviceConnectivity {
    
    //Public
    
    public func start() {
        
    }
    
    public func sendData_iPhone(dictInfo: Dictionary<String, Any>) {
        
        do {
            _ = try self.session?.updateApplicationContext(dictInfo)
            
        } catch let error {
            
            print(error.localizedDescription)
        }
    }
    
    public func sendData_iWatch(dictInfo: Dictionary<String, Any>) {
        
        do {
            _ = try self.session?.updateApplicationContext(dictInfo)
            
        } catch let error {
            
            print(error.localizedDescription)
        }
    }
}

extension DeviceConnectivity {
    
    //MARK: Private
    
    private func setupWCSession() {
        
        if WCSession.isSupported() {
            
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        }
    }
}

extension DeviceConnectivity: WCSessionDelegate {
    
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if activationState == .activated {
            print("counter part app is active")
        } else if activationState == .notActivated {
            print("counter part app is not active")

        }
    }
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        
        let activationState = session.activationState
        if activationState == .activated {
            print("counter part app is active")
        } else if activationState == .notActivated {
            print("counter part app is not active")
        }
    }
    

     #if os(iOS)
    public func sessionDidBecomeInactive(_ session: WCSession) {
        
        print("Session Inactive")
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
        
        print("Session Deactivate")
    }
    #endif
    
    public func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        
        //TODO: Check data for iphone or for watch
        self.arrData.append(applicationContext)
        print(arrData.count)
        CoreDataStorage.shared.saveAllData(dictInfo: applicationContext)
    }
    
}
