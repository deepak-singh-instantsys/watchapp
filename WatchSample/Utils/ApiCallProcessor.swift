//
//  ApiCallProcessor.swift
//  WatchSample
//
//  Created by Deepak on 08/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit

protocol ApiCallProcessorEvent: class {
    
    func didDataUploadSuccessfully(dictData: Dictionary<String, Any>) -> Void
    func didDataUploadFailed(dictData: Dictionary<String, Any>) -> Void
}

protocol ApiCallProcessorProtocols {
    
    weak var delegate: ApiCallProcessorEvent? {set get}
    func uploadData(dictData: Dictionary<String, Any>) -> Void
}

class ApiCallProcessor: NSObject, ApiCallProcessorProtocols {
    
    weak var delegate: ApiCallProcessorEvent?
    var counter = 0


    
    override init() {
        super.init()
    }
    
    deinit {
        print("ApiCallProcessor-deinit")
    }
    
    func getJSON(data: Data) -> [Dictionary<String, Any>]? {
        
        do {
            let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let arrRecords = result as? [Dictionary<String, Any>] {
                return arrRecords
            }
        } catch {
        }
        
        return nil
    }

    
    func uploadData(dictData: Dictionary<String, Any>) {
        
        if let data = dictData[KDATA] as? Data {
    
            let request = NSMutableURLRequest(url: NSURL(string: "https://h18wh7cxmd.execute-api.us-east-1.amazonaws.com/prod/appleWatchDataServiceTest")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 30.0)
            request.httpMethod = "POST"
            
            request.httpBody = data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                
                     self.delegate?.didDataUploadFailed(dictData: dictData)
                    
                } else {
                    
                    
                    do {
                        let result = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        self.delegate?.didDataUploadSuccessfully(dictData: dictData)
                        
                    } catch {
                         print(error.localizedDescription)
                         self.delegate?.didDataUploadFailed(dictData: dictData)
                    }
                }
            })
            
            dataTask.resume()
        } else {
            self.delegate?.didDataUploadFailed(dictData: dictData)
        }
    }
   
    
                // Barry's server
    /*func uploadData(dictData: Dictionary<String, Any>) {
        

        if let data = dictData[KDATA] as? Data {
            
            let url = URL(string: KBaseUrl + KAPI_Sensors)
            var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
            request.httpMethod = HTTPMethodType.POST.rawValue
            request.httpBody = data
            
            let user = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KUSERNAME))
            let password = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KPASSWORD))

             let plainString = "\(user):\(password)" as NSString
             let plainData = plainString.data(using: String.Encoding.utf8.rawValue)
             let base64String = plainData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
             let autorization = "Basic " + base64String
            
            let headers = [
                "authorization": autorization,
                "content-type": "application/octet-stream"
            ]
            
            request.allHTTPHeaderFields = headers
            let configuration = URLSessionConfiguration.default
            configuration.urlCache = nil
            let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)

            let task = session.dataTask(with: request, completionHandler: { (responseData, response, error) in
                
                URLCache.shared.removeAllCachedResponses()

                if(error == nil && responseData != nil) {
                    
                    do {
                        let result = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                        print(result)
                        if let responseDict = result as? Dictionary<String, Any>, let status = responseDict["status"] as? String, status == "1" {
                            
                            self.delegate?.didDataUploadSuccessfully(dictData: dictData)
                            
                        } else {
                            self.delegate?.didDataUploadFailed(dictData: dictData)
                        }
                        
                    } catch {
                        
                        print(error.localizedDescription)
                        self.delegate?.didDataUploadFailed(dictData: dictData)
                    }
                    
                } else {
                    
                    print(error?.localizedDescription ?? "")
                    self.delegate?.didDataUploadFailed(dictData: dictData)
                }
            })
            
            task.resume();
        } else {
            self.delegate?.didDataUploadFailed(dictData: dictData)
        }
    }*/
}

extension ApiCallProcessor: URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            
            if challenge.protectionSpace.host == "beta-factorlab.us-east-1.elasticbeanstalk.com" {
                
                let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
            }
        }
    }
}
