//
//  Define.swift
//  Sensors
//
//  Created by Deepak on 11/7/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit


let KNotification_Auth_Received     = "NotificationNameAuthReceived"
#if os(iOS)
let KSharedAppDelegate              = UIApplication.shared.delegate as! AppDelegate
#endif
let KUserDefault                    = UserDefaults.standard


let KAPPName                    = "WatchApp"
let KDirectoryName_Sensor       = "Sensor_Data"
let KUUID                       = "uuid"
let KDATA                       = "data"
let KLocalFileURL               = "localFileURL"
let KISSynch                    = "isSynchWithServer"
let KConnectedWithWatch         = "isUserIdSendtowatch"
let KUSERNAME                   = "username"
let KPASSWORD                   = "password"
let KWATCH_AUTH                 = "watch_auth"
let KLogedIn                    = "logedIn"
let KMESSAGE_TYPE               = "messageType"
let KMESSAGE_TYPE_AUTH          = "auth"
let KMESSAGE_TYPE_AUTHACK       = "authACK"

let KSTATUS                     = "status"
let KMAX_LIMIT_UNPROCESS_DATA_COUNT = "unprocess_max_data_count"

let KMESSAGE_TYPE_AUTH_REQUEST        = "authRequest"
let KMESSAGE_TYPE_SENSOR_OBSERVATION  = "sensor_observation_data"



/********** Server keys ***********/
let Kactivity                = "activity"
let Kaltitude                = "altitude"
let Kcadence                 = "cadence"
let Kdistance                = "distance"
let Kflight_asc              = "flight_asc"
let Kflight_desc               = "flight_desc"
let Kheading_angle           = "heading_angle"
let Kheart_rate              = "heart_rate"
let Klatitude                = "latitude"
let Klongitude               = "longitude"
let Kpace                    = "pace"
let Kpresure                 = "pressure"
let Krotation_angle          = "rotation_angle"
let Ksteps                   = "steps"
let Ktemperature             = "temperature"
let Ktime_stamp              = "time_stamp"
let Kx_quaternion            = "x_quaternion"
let Ky_quaternion            = "y_quaternion"
let Kz_quaternion            = "z_quaternion"
let Kw_quaternion            = "w_quaternion"
let Kx_acceleration          = "x_acceleration"
let Ky_acceleration          = "y_acceleration"
let Kz_acceleration          = "z_acceleration"
let Kx_rotationRate          = "x_rotationRate"
let Ky_rotationRate          = "y_rotationRate"
let Kz_rotationRate          = "z_rotationRate"
let Kx_user_accel            = "x_user_accel"
let Ky_user_accel            = "y_user_accel"
let Kz_user_accel            = "z_user_accel"
