//
//  APIClient.swift
//  Demo
//
//  Created by Deepak Singh on 26/09/17.
//  Copyright © 2017 Deepak Singh. All rights reserved.
//

import UIKit



let KAPI_Upload_Sensor_Data = "https://h18wh7cxmd.execute-api.us-east-1.amazonaws.com/prod/appleWatchDataServiceTest"


/*let KBaseUrl_local = "http://192.168.1.41:8080/factorlab-web/ws/"
let KBaseUrl = "https://beta-factorlab.us-east-1.elasticbeanstalk.com/ws/"
let KAPI_Sensors = "v2/sensors"
let KAPI_Login = "security/mobileAuth"*/






enum HTTPMethodType: String {
    case GET  = "GET"
    case POST = "POST"
}


class APIClient: NSObject, URLSessionDelegate {
    
    //MARK: Public
    
    static let shared: APIClient = {
        let instance = APIClient()
        return instance
    }()
    

    public  func uploaddata(apiURL: String,data: Data, apiCompletionBlock:@escaping (_ dictResponse:Dictionary<String,Any>?, _ error: Error?)->Void)
    {
        
        let url = URL(string: apiURL)
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        request.httpMethod = HTTPMethodType.POST.rawValue
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type");
        request.addValue("application/json",forHTTPHeaderField: "Accept");
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request, completionHandler: { (responseData, response, error) in
            
            if(error == nil && responseData != nil) {
                
                do {
                    let result = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                    
                    if let dictResponse = result as? Dictionary<String, Any> {
                        
                        apiCompletionBlock(dictResponse,nil)
                        
                    } else {
                        
                        apiCompletionBlock(nil,error)
                    }
                    
                } catch {
                    
                    apiCompletionBlock(nil,error)
                }
                
            } else {
                
                apiCompletionBlock(nil,error)
            }
        })
        
        task.resume();
    }
    
    
    
    public  func requestGetAPI(apiURL: String, httpMethoType type: HTTPMethodType, apiCompletionBlock:@escaping (_ dictResponse:Dictionary<String,Any>?, _ error: Error?)->Void)
    {
        
        let url = URL(string: apiURL)
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        request.httpMethod = HTTPMethodType.GET.rawValue
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type");
        request.addValue("application/json",forHTTPHeaderField: "Accept");
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request, completionHandler: { (responseData, response, error) in
            
            if(error == nil && responseData != nil) {
                
                do {
                    let result = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                    
                    if let dictResponse = result as? Dictionary<String, Any> {
                        
                        apiCompletionBlock(dictResponse,nil)
                        
                    } else {
                        
                        apiCompletionBlock(nil,error)
                    }
                    
                } catch {
                    
                    apiCompletionBlock(nil,error)
                }
                
            } else {
                
                apiCompletionBlock(nil,error)
            }
        })
        
        task.resume();
    }
    
   /* public func uploadDataToServer(data: Data, onCompletion:@escaping (_ success: Bool) -> Void)-> Void {
        
        let url = URL(string: KBaseUrl + KAPI_Sensors)
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        request.httpMethod = HTTPMethodType.POST.rawValue
        
        request.httpBody = data
        
        let headers = [
            "authorization": "Basic dmlrYXNAZGZkZW1vLmNvbTpncm93MTIzNA==",
            "content-type": "application/octet-stream"
        ]
        
        request.allHTTPHeaderFields = headers
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request, completionHandler: { (responseData, response, error) in
            
            if(error == nil && responseData != nil) {
                
                do {
                    let result = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                    print(result)
                    if let responseDict = result as? Dictionary<String, Any>, let status = responseDict["status"] as? String, status == "1" {
                        onCompletion(true)
                    } else {
                        onCompletion(false)
                    }
                    
                } catch {
                    
                    print(error.localizedDescription)
                    onCompletion(false)
                }
                
            } else {
                
                onCompletion(false)
            }
        })
        
        task.resume();
    }*/
    
   /* public func login(userName: String, password: String ,onCompletion:@escaping (_ dictRes: Any?) -> Void)-> Void {
        
        let url = URL(string: KBaseUrl + KAPI_Login)
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        request.httpMethod = HTTPMethodType.POST.rawValue
        print(url ?? "")
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "Accept" : "application/json"
        ]

        let paramString = "username=" + userName + "&" + "password=" + password
        let data = paramString.data(using: .utf8)
        
        request.httpBody = data
        request.allHTTPHeaderFields = headers
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: { (responseData, response, error) in
            
            if(error == nil && responseData != nil) {
                
                do {
                    let result = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                    print(result)
                    
                    onCompletion(result)

                } catch {
                    
                    print(error.localizedDescription)
                    onCompletion(nil)
                }
                
            } else {
                
                onCompletion(nil)
            }
        })
        task.resume();
    }*/
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            
            if challenge.protectionSpace.host == "beta-factorlab.us-east-1.elasticbeanstalk.com" {
                
                let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
            }
        }
    }
}

extension APIClient {
    
    //MARK: Private
    
    fileprivate  func getDataFromParameter(dictParam: Dictionary<String, Any>) -> Data? {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictParam, options: .prettyPrinted)
            return jsonData
            
        } catch {
            
            return nil
        }
    }
}
