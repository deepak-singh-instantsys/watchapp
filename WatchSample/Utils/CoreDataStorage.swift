//
//  CoreDataStorage.swift
//  Sensors
//
//  Created by Deepak on 11/13/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStorage: NSObject {

    private var arrIncommingData = [Dictionary<String, Any>]()
    private let priority = DispatchQoS.background
    private var backgroundQueue: DispatchQueue?
    private let deadline = DispatchTime.now() + .seconds(10)

    
    private var saveRecordSerialQueue =  OperationQueue()
    
    public static let shared: CoreDataStorage = {
       
        return CoreDataStorage()
    }()
    
    private override init() {
        super.init()
        saveRecordSerialQueue.maxConcurrentOperationCount = 1
        self.saveDataEvery30Seconds()
    }
}

extension CoreDataStorage {
    
    //MARK: Private
    
    
    private  func saveDataEvery30Seconds() {
        CoreDataManager.saveContext(CoreDataManager.sharedInstance.privateQueueCtxt)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            
            self.saveDataEvery30Seconds()
        }
    }
}

extension CoreDataStorage {
    
    public func saveRecord(dictInfo: Dictionary<String, Any>) {
        
       /* let moc             = CoreDataManager.sharedInstance.privateQueueCtxt
        let entity          = NSEntityDescription.insertNewObject(forEntityName: "AllData", into: moc!) as! AllData
        
        
    
        
        if let timeStamp = dictInfo[Ktime_stamp] as? Double {
            
            entity.time_stamp = Double(timeStamp)
        }
        
        entity.x_user_accel   = CommonHelper.convertToString(obj: dictInfo[Kx_user_accel])
        entity.y_user_accel   = CommonHelper.convertToString(obj: dictInfo[Ky_user_accel])
        entity.z_user_accel  = CommonHelper.convertToString(obj: dictInfo[Kz_user_accel])
       
        entity.x_rotationRate       = CommonHelper.convertToString(obj: dictInfo[Kx_rotationRate])
        entity.x_rotationRate       = CommonHelper.convertToString(obj: dictInfo[Ky_rotationRate])
        entity.x_rotationRate       = CommonHelper.convertToString(obj: dictInfo[Kz_rotationRate])
        
        
        entity.xQuaternion     = CommonHelper.convertToString(obj: dictInfo[KxQuaternion])
        entity.yQuaternion     = CommonHelper.convertToString(obj: dictInfo[KyQuaternion])
        entity.zQuaternion     = CommonHelper.convertToString(obj: dictInfo[KzQuaternion])
        entity.wQuaternion     = CommonHelper.convertToString(obj: dictInfo[KwQuaternion])
        
        entity.activity      = CommonHelper.convertToString(obj: dictInfo[kActivity])
        entity.cadence       = CommonHelper.convertToString(obj: dictInfo[KCadence])
        entity.distance      = CommonHelper.convertToString(obj: dictInfo[KDistance])
        entity.flightAsc     = CommonHelper.convertToString(obj: dictInfo[KFlightAsc])
        entity.flightdsc     = CommonHelper.convertToString(obj: dictInfo[KFlightdsc])
        entity.heartRate     = CommonHelper.convertToString(obj: dictInfo[KHeartRate])
        entity.pace          = CommonHelper.convertToString(obj: dictInfo[KPace])
        entity.steps         = CommonHelper.convertToString(obj: dictInfo[KSteps])
        entity.latitude      = CommonHelper.convertToString(obj: dictInfo[KLatitude])
        entity.longitude     = CommonHelper.convertToString(obj: dictInfo[KLongitude])
        entity.presure       = CommonHelper.convertToString(obj: dictInfo[kPresure])
        entity.altitude      = CommonHelper.convertToString(obj: dictInfo[kAltitude])
        entity.rotationAngle = CommonHelper.convertToString(obj: dictInfo[KRotationAngle])
        entity.headingAngle  = CommonHelper.convertToString(obj: dictInfo[KHeadingAngle])*/
    
    }
    
    
    
    public func saveRecordsInBatch(arrRecords: [Dictionary<String, Any>], onCompletion:@escaping () -> Void) {
    
       /* let moc = CoreDataManager.sharedInstance.privateQueueCtxt
        let group = DispatchGroup()
        
        
        for dictInfo in arrRecords {
            
            let blockOperation = BlockOperation()
            group.enter()
            
            blockOperation.addExecutionBlock({
                
                let entity = NSEntityDescription.insertNewObject(forEntityName: "AllData", into: moc!) as! AllData
                
                entity.isSynchWithServer = "0"
            
                
                if let timeStamp = dictInfo[KTimeStamp] as? Double {
                    
                    entity.timeStamp    =   Double(timeStamp)
                }
                
                entity.xUser_acceleration   = CommonHelper.convertToString(obj: dictInfo[KxUser_acceleration])
                entity.yUser_acceleration   = CommonHelper.convertToString(obj: dictInfo[KyUser_acceleration])
                entity.zUser_acceleration  = CommonHelper.convertToString(obj: dictInfo[KzUser_acceleration])
                entity.xRotationRate       = CommonHelper.convertToString(obj: dictInfo[KxrotationRate])
                entity.yRotationRate       = CommonHelper.convertToString(obj: dictInfo[KyrotationRate])
                entity.zRotationRate       = CommonHelper.convertToString(obj: dictInfo[KzrotationRate])
                entity.xQuaternion     = CommonHelper.convertToString(obj: dictInfo[KxQuaternion])
                entity.yQuaternion     = CommonHelper.convertToString(obj: dictInfo[KyQuaternion])
                entity.zQuaternion     = CommonHelper.convertToString(obj: dictInfo[KzQuaternion])
                entity.wQuaternion     = CommonHelper.convertToString(obj: dictInfo[KwQuaternion])
                
                entity.activity      = CommonHelper.convertToString(obj: dictInfo[kActivity])
                entity.cadence       = CommonHelper.convertToString(obj: dictInfo[KCadence])
                entity.distance      = CommonHelper.convertToString(obj: dictInfo[KDistance])
                entity.flightAsc     = CommonHelper.convertToString(obj: dictInfo[KFlightAsc])
                entity.flightdsc     = CommonHelper.convertToString(obj: dictInfo[KFlightdsc])
                entity.heartRate     = CommonHelper.convertToString(obj: dictInfo[KHeartRate])
                entity.pace          = CommonHelper.convertToString(obj: dictInfo[KPace])
                entity.steps         = CommonHelper.convertToString(obj: dictInfo[KSteps])
                entity.latitude      = CommonHelper.convertToString(obj: dictInfo[KLatitude])
                entity.longitude     = CommonHelper.convertToString(obj: dictInfo[KLongitude])
                entity.presure       = CommonHelper.convertToString(obj: dictInfo[kPresure])
                entity.altitude      = CommonHelper.convertToString(obj: dictInfo[kAltitude])
                entity.rotationAngle = CommonHelper.convertToString(obj: dictInfo[KRotationAngle])
                entity.headingAngle  = CommonHelper.convertToString(obj: dictInfo[KHeadingAngle])
              
                entity.surroundingTempreture = CommonHelper.convertToString(obj: dictInfo[KTemperature])
                entity.xAccelAccelerometer = CommonHelper.convertToString(obj: dictInfo[KxAccel_Accelerometer])
                entity.yAccelAccelerometer = CommonHelper.convertToString(obj: dictInfo[KyAccel_Accelerometer])
                entity.zAccelAccelerometer = CommonHelper.convertToString(obj: dictInfo[KzAccel_Accelerometer])

                group.leave()
            })
            
            self.saveRecordSerialQueue.addOperation(blockOperation)
        }
        
        group.notify(queue: DispatchQueue.main) {
         
           CoreDataManager.saveContext(moc)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
                
                onCompletion()
            })
        }
 */
    }
    
    
    public func deleteAllRecords(onCompletion:(_ success: Bool)->Void) {
        
        let moc = CoreDataManager.sharedInstance.privateQueueCtxt
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AllData")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
       
        do {
            _ = try moc?.execute(deleteRequest)
            CoreDataManager.saveContext(moc)
            onCompletion(true)
        } catch let error {
            
            print(error.localizedDescription)
            onCompletion(false)
        }
    }
}

