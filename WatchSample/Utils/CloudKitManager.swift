//
//  YALCloudKitManager.swift
//  CloudKit-demo
//
//  Created by Maksim Usenko on 3/25/15.
//  Copyright (c) 2015 Yalantis. All rights reserved.
//

import UIKit
import CloudKit

private let recordType = "SensorData"

 class CloudKitManager: NSObject {
    
    var deleteRecordSerialQueue = OperationQueue()
    
    
    static let shared: CloudKitManager = {
        return CloudKitManager()
    }()
    
    
    private override init() {
        super.init()
        self.initializeManager()
    }
    
    
    static var publicCloudDatabase: CKDatabase {
        
        let  container = CKContainer(identifier: "iCloud.instantsys.sensors")
        let publicDB = container.publicCloudDatabase
        return publicDB
    }
}

extension CloudKitManager {
    
    public func checkLoginStatus(_ handler: @escaping (_ islogged: Bool) -> Void) {
        
        CKContainer.default().accountStatus{ accountStatus, error in
            if let error = error {
                print(error.localizedDescription)
            }
            switch accountStatus {
            case .available:
                handler(true)
            default:
                handler(false)
            }
        }
    }

    
    public  func createRecordAsData(_ data: Data, completion: @escaping (_ record: CKRecord?, _ error: Error?) -> Void) {
        
         let record = CKRecord(recordType: recordType)
         record.setValue(data, forKey: KDATA)
         record.setValue(0, forKey: KISSynch)
        
        CloudKitManager.publicCloudDatabase.save(record) { (record, error) in
            
            completion(record, error)
        }
    }
    
    
    public  func createRecord(_ dictInfo: Dictionary<String, Any>, completion: @escaping (_ record: CKRecord?, _ error: Error?) -> Void) {
        
        let record = CKRecord(recordType: recordType)
        
        record.setValue("0", forKey: "isSynchWithServer")
        
       
        
        if let timeStamp = dictInfo[Ktime_stamp] as? Double {
            record.setValue(Int64(Double(timeStamp)), forKey: Ktime_stamp)
        }
        let xAcceleration = CommonHelper.convertToString(obj: dictInfo[Kx_acceleration])
        let yAcceleration = CommonHelper.convertToString(obj: dictInfo[Ky_acceleration])
        let zAcceleration = CommonHelper.convertToString(obj: dictInfo[Kz_acceleration])
        
        let xRotationRate       = CommonHelper.convertToString(obj: dictInfo[Kx_rotationRate])
        let yRotationRate       = CommonHelper.convertToString(obj: dictInfo[Ky_rotationRate])
        let zRotationRate       = CommonHelper.convertToString(obj: dictInfo[Kz_rotationRate])
        
        let xQuaternion     = CommonHelper.convertToString(obj: dictInfo[Kx_quaternion])
        let yQuaternion     = CommonHelper.convertToString(obj: dictInfo[Ky_quaternion])
        let zQuaternion     = CommonHelper.convertToString(obj: dictInfo[Kz_quaternion])
        let wQuaternion = CommonHelper.convertToString(obj: dictInfo[Kw_quaternion])

        
        let activity      = CommonHelper.convertToString(obj: dictInfo[Kactivity])
        let cadence       = CommonHelper.convertToString(obj: dictInfo[Kcadence])
        let distance      = CommonHelper.convertToString(obj: dictInfo[Kdistance])
        let flightAsc     = CommonHelper.convertToString(obj: dictInfo[Kflight_desc])
        let flightdsc     = CommonHelper.convertToString(obj: dictInfo[Kflight_desc])
        let heartRate     = CommonHelper.convertToString(obj: dictInfo[Kheart_rate])
        let pace          = CommonHelper.convertToString(obj: dictInfo[Kpace])
        let steps         = CommonHelper.convertToString(obj: dictInfo[Ksteps])
        let latitude      = CommonHelper.convertToString(obj: dictInfo[Klatitude])
        let longitude     = CommonHelper.convertToString(obj: dictInfo[Klongitude])
        let presure       = CommonHelper.convertToString(obj: dictInfo[Kpresure])
        let altitude      = CommonHelper.convertToString(obj: dictInfo[Kaltitude])
        let rotationAngle = CommonHelper.convertToString(obj: dictInfo[Krotation_angle])
        let headingAngle  = CommonHelper.convertToString(obj: dictInfo[Kheading_angle])

        
        /*record.setValue(activity, forKey: kActivity)
        record.setValue(cadence, forKey: KCadence)
        record.setValue(distance, forKey: KDistance)
        record.setValue(flightAsc, forKey: KFlightAsc)
        record.setValue(flightdsc, forKey: KFlightdsc)
        record.setValue(heartRate, forKey: KHeartRate)
        record.setValue(pace, forKey: KPace)
        record.setValue(steps, forKey: KSteps)
        record.setValue(latitude, forKey: KLatitude)
        record.setValue(longitude, forKey: KLongitude)
        record.setValue(presure, forKey: kPresure)
        record.setValue(altitude, forKey: kAltitude)
        record.setValue(rotationAngle, forKey: KRotationAngle)
        record.setValue(headingAngle, forKey: KHeadingAngle)
        record.setValue(xAcceleration, forKey: KxUser_acceleration)
        record.setValue(yAcceleration, forKey: KyUser_acceleration)
        record.setValue(zAcceleration, forKey: KzUser_acceleration)
        record.setValue(xRotationRate, forKey: KxrotationRate)
        record.setValue(yRotationRate, forKey: KyrotationRate)
        record.setValue(zRotationRate, forKey: KzrotationRate)
        record.setValue(xQuaternion, forKey: KxQuaternion)
        record.setValue(yQuaternion, forKey: KyQuaternion)
        record.setValue(zQuaternion, forKey: KzQuaternion)
        record.setValue(wQuaternion, forKey: KwQuaternion)*/

        
        CloudKitManager.publicCloudDatabase.save(record) { (record, error) in
            
            completion(record, error)
        }
    }
    
    public  func fetchRecord(_ onCompletion: @escaping (_ records: [CKRecord], _ error: Error?) -> Void) {
        
         let predicate = NSPredicate.init(format: "isSynchWithServer == 0")
         let query = CKQuery(recordType: recordType, predicate: predicate)
        
        CloudKitManager.publicCloudDatabase.perform(query, inZoneWith: nil) { (records, error) in
            
            if records != nil && error == nil {
                
               onCompletion(records!, error)
                
            } else {
                
                 onCompletion([] , error)
            }
        }
    }
    

    public func deleteBatchRecords(records: [CKRecord], onBatchCompletion:@escaping ()-> Void) {
        
        let group = DispatchGroup()
        
        for record in records {
           
            let operationBlock = BlockOperation()
            group.enter()
            operationBlock.addExecutionBlock({
                
                let recordId = record.recordID
                CloudKitManager.publicCloudDatabase.delete(withRecordID: recordId, completionHandler: { deletedRecordId, error in
                    
                    if error == nil && deletedRecordId != nil {
                        
                        print("Record removed from cloud")
                        
                    } else {
                        print("Record can not be removed from cloud")
                    }
                    
                    group.leave()
                })
            })
            
            self.deleteRecordSerialQueue.addOperation(operationBlock)
        }
        
        
        group.notify(queue: DispatchQueue.global()) {
            
            onBatchCompletion()
        }
    }
    
    public func updateRecord(_ record: CKRecord, newValue: String, key: String,onCompletion: @escaping (CKRecord?, Error?) -> Void) {
        
            record.setValue(newValue, forKey: key)
            CloudKitManager.publicCloudDatabase.save(record) { savedRecord, error in
                
                if error == nil && savedRecord != nil {
                    
                    onCompletion(savedRecord, error)
            }
        }
    }
    
    public class func getArrayOfDictionaryInfo(records: [CKRecord]) -> [Dictionary<String, Any>] {
        
        var arrDictInfo = [Dictionary<String, Any>]()
        for recordData in records {
            
            if let data = recordData.value(forKey: KDATA) as? Data {
                
                /*if let arrDict =  NSKeyedUnarchiver.unarchiveObject(with: data) as? [Dictionary<String, Any>] {
                    
                    arrDictInfo = arrDictInfo + arrDict
                }*/
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let arrDict = arrResult as? [Dictionary<String, Any>] {
                        
                        arrDictInfo = arrDictInfo + arrDict
                    }
                } catch {
                
                }
            }
        }
        
        return arrDictInfo
    }
}

extension CloudKitManager {
    
        //MARK: Private
    
    
    private func initializeManager() {
        
        self.deleteRecordSerialQueue.maxConcurrentOperationCount = 1
    }
    
    private func deleteRecord(record: CKRecord, onCompletion:@escaping (_ success: Bool)-> Void) {
        
        let recordId = record.recordID
        CloudKitManager.publicCloudDatabase.delete(withRecordID: recordId, completionHandler: { deletedRecordId, error in
            
            if error == nil && deletedRecordId != nil {
                onCompletion(true)
            } else {
                onCompletion(false)
            }
        })
    }
}
