//
//  DataProcessor.swift
//  WatchSample
//
//  Created by Deepak on 11/23/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit


let MAX_LIMIT_UNPROCESS_DATA_COUNT  = 300

class DataProcessor: NSObject {

    private var arrUnProcesse = [Dictionary<String, Any>]()
    private var arrInProcess = [Dictionary<String, Any>]()

    
    private var diskOperationSerialQueue:DispatchQueue? = DispatchQueue(label: "diskOperationSerialQueue")
    private var cloudOperationSerialQueue:DispatchQueue = DispatchQueue(label: "CloudOperationSerialQueue")
    
    private var DURATION_FOR_DOCUMENT_DIRECTORY_CHECKER = 10.0
    private var fileOperationProcessor: FileManagerProcessor?
    
    static let shared: DataProcessor = {
       return DataProcessor()
    }()

    
    private override init() {
        super.init()
        
        self.initializeFileManagerProcessor()
        self.checkDataAvailableOnDocumentDirtory()
    }
    public func start() {
        
    }
    
    public func didReceiveDataFromSensors(dicInfo: Dictionary<String, Any>) {
        
        self.arrUnProcesse.append(dicInfo)
        
        if self.arrUnProcesse.count > MAX_LIMIT_UNPROCESS_DATA_COUNT {
            
            let arrTempUnProcessData = self.arrUnProcesse
            self.arrUnProcesse.removeAll()
            self.saveSensorDictionaryInfo(sensorInfo: arrTempUnProcessData)
        }
    }
}

extension DataProcessor {
    
    private func saveSensorDictionaryInfo(sensorInfo: [Dictionary<String, Any>]) {
        
        self.fileOperationProcessor?.saveArrayOfSensorInfoAsDataOnDocumentDirectory(arrRecords: sensorInfo)
    }
    
    private func checkDataAvailableOnDocumentDirtory() {
        
        self.isNetworkAvailable { [weak self] (sucess)  in
            
            if sucess {
                
                self?.fileOperationProcessor?.getChunksOfDataFromDirectory()
                
            } else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
                    
                    self?.checkDataAvailableOnDocumentDirtory()
                }
            }
        }
    }
    
    
    private func checUnProcesseDataAvailableOnMemory() {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//
//             self.processNextSegment_toSaveLocally()
//        }
    }

    private func isNetworkAvailable(onCheck:@escaping (_ success: Bool)-> Void) {
        
        let url = URL(string: "http://www.google.com")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            if data != nil && error == nil {
                onCheck(true)
            } else {
                onCheck(false)
            }
        }
        task.resume()
    }
    
    private func initializeFileManagerProcessor() {
        
        self.fileOperationProcessor = FileManagerProcessor()
        self.fileOperationProcessor?.delegate = self
    }
    
    private func starProcessToUpload() {
        
        if self.arrInProcess.count > 0 {
            
            print("*** Batch upload Count = \(self.arrInProcess.count)***")
            let apiCall = ApiCallProcessor()
            apiCall.delegate = self
            apiCall.uploadData(dictData: self.arrInProcess.first!)
            
        } else {
            
            print("***Batch upload Finished = \(self.arrInProcess.count)***")
            self.checkDataAvailableOnDocumentDirtory()
        }
    }
}


extension DataProcessor: FileManagerProcessorEvent {
    
            //MARK: FileManagerProcessorEvent
    
    func didDataRemoved(dictData: Dictionary<String, Any>) {
        
         print("Data removed from document directory...")
    }
    
    
    func didReceivedChunksOfDataFromDirectory(arrChunksData: [Dictionary<String, Any>]) -> Void {
        
        self.arrInProcess = arrChunksData
        self.starProcessToUpload()
        
    }
    
    func didDirectoryEmpty() -> Void {
        
        print("No data available on document directory to upload...")
        DispatchQueue.main.asyncAfter(deadline: .now() + DURATION_FOR_DOCUMENT_DIRECTORY_CHECKER) {
            
            self.checkDataAvailableOnDocumentDirtory()
        }
    }
    
    func didsaveDataOnDirectory() -> Void {
        
       // print("***Data saved on document directory***")
    }
    
    func didFailToSaveDataOnDirectory(error: Error?) {
     
        print("did Fail To Save Data On Directory=",error?.localizedDescription ?? "")
    }
    
    func didDataRemovedFailed(error: Error?) -> Void {
        
        print("Data remove from document directory failed =",error?.localizedDescription ?? "")
    }
}

extension DataProcessor: ApiCallProcessorEvent {
    
    func didDataUploadFailed(dictData: Dictionary<String, Any>) {
        
       print("***did Data Upload Failed***")
        self.arrInProcess.removeFirst()
        
        DispatchQueue.main.asyncAfter(deadline: .now() +  2) {
            self.starProcessToUpload()
        }
    }
    
    func didDataUploadSuccessfully(dictData: Dictionary<String, Any>) {
        
         print("***did Data Upload Successfully***")
         self.fileOperationProcessor?.removeData(dictData: dictData)
         self.arrInProcess.removeFirst()
         self.starProcessToUpload()

    }
}


