//
//  CloudDataProcessor.swift
//  WatchSample
//
//  Created by Deepak on 11/23/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit

protocol CloudDataProcessorEvent: class {
    
    func didDataUploadFinishEvent(dictData: Dictionary<String, Any>) -> Void
    func didDataUploadFailed(dictData: Dictionary<String, Any>) -> Void
    func didBatchUploadComplete() -> Void
}


protocol CloudDataProcessorProtocol: class {
    
    //CloudDataProcessor -->> DataProcessor
    weak var delegate: CloudDataProcessorEvent? {set get}
    func submitDataForCloudProcess(arrData: [Dictionary<String, Any>]) -> Void
    func cancelUploadProcess() -> Void
}

class CloudDataProcessor: NSObject, CloudDataProcessorProtocol {
    
    var delegate: CloudDataProcessorEvent?
    private var uploadOperationSerailQueue = OperationQueue()
    private var arrDataInProcess = [Dictionary<String, Any>]()
    
    override init() {
        
        super.init()
        self.uploadOperationSerailQueue.maxConcurrentOperationCount = 1
    }
    
    func submitDataForCloudProcess(arrData: [Dictionary<String, Any>]) -> Void {
        
        let group = DispatchGroup()
        for dictData in arrData {
            
            let blockOperation = BlockOperation()
            group.enter()
            blockOperation.addExecutionBlock({
                
                if let data = dictData[KDATA] as? Data {
                
                    
                   /* CloudKitManager.shared.createRecordAsData(data, completion: { (record, error) in
                        
                        print("Data uploaded on cloud=",counter)

                        counter += 1
                        
                        if error == nil && record != nil {
                            self.delegate?.didDataUploadFinishEvent(dictData: dictData)
                            
                        } else {
                            
                            print("upload on iCloud - error received",error?.localizedDescription ?? "")
                            
                            self.delegate?.didDataUploadFailed(dictData: dictData)
                        }
                        
                         group.leave()
                    })*/
                    
                } else {
                    
                    group.leave()
                }
        })
            
        self.uploadOperationSerailQueue.addOperation(blockOperation)
    }
        
        group.notify(queue: DispatchQueue.main) {
            
           self.delegate?.didBatchUploadComplete()
        }
    }
    
    func cancelUploadProcess() -> Void {
        
    }
}

extension CloudDataProcessor {
        //MARK: Private
    
}











