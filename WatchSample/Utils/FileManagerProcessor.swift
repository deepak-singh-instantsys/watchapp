//
//  FileManagerProcessor.swift
//  WatchSample
//
//  Created by Deepak on 11/23/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit

protocol FileManagerProcessorEvent: class {

    func didReceivedChunksOfDataFromDirectory(arrChunksData: [Dictionary<String, Any>]) -> Void
    func didDirectoryEmpty() -> Void
    func didsaveDataOnDirectory() -> Void
    func didFailToSaveDataOnDirectory(error: Error?) -> Void
    func didDataRemoved(dictData: Dictionary<String, Any>) -> Void
    func didDataRemovedFailed(error: Error?) -> Void
}

protocol FileManagerProcessorProtocols {
    
    weak var delegate: FileManagerProcessorEvent? {set get}
    func getChunksOfDataFromDirectory() -> Void
    func saveArrayOfSensorInfoAsDataOnDocumentDirectory(arrRecords: [Dictionary<String, Any>]) -> Void
    func removeData(dictData: Dictionary<String, Any>)-> Void

}

class FileManagerProcessor: NSObject, FileManagerProcessorProtocols {
    
    private var saveDataOperationSerailQueue = OperationQueue()
    private var getDataOperationSerailQueue = OperationQueue()
    private var removeDataOperationSerailQueue = OperationQueue()
    
    weak var delegate: FileManagerProcessorEvent?
    
    override init() {
        super.init()
        
        self.createDirectoryWithNameIfNeeded(folderName: KDirectoryName_Sensor)
        self.getDataOperationSerailQueue.maxConcurrentOperationCount = 1
        self.saveDataOperationSerailQueue.maxConcurrentOperationCount = 1
        self.removeDataOperationSerailQueue.maxConcurrentOperationCount = 1
    }
    
    func getChunksOfDataFromDirectory() -> Void {
        
        
        let urls = self.getListOfFileURLFromDictory(dicrectoryName: KDirectoryName_Sensor)
        if urls.count > 0 {
            
            var pickUpURL = [URL]()
            
            if urls.count >= 3 {
                /*******
                 - Do not pickup entire data chunks one at time from directory, this may lead to memory warnning
                 - pick up first 10 data chunks
                 *************/
                let arraySlice = urls.prefix(upTo: 3)
                pickUpURL = Array(arraySlice)
                
            } else {
                pickUpURL = urls
            }
            
            self.getDataFromUrl(arrUrl: pickUpURL) { [weak self] (arrDictData) in
                
                self?.delegate?.didReceivedChunksOfDataFromDirectory(arrChunksData: arrDictData)
                
            }
        } else {
            self.delegate?.didDirectoryEmpty()
        }
    }
    
    func saveArrayOfSensorInfoAsDataOnDocumentDirectory(arrRecords: [Dictionary<String, Any>]) -> Void {
        
        // let data = NSKeyedArchiver.archivedData(withRootObject: arrRecords)
        do {
            
           let data = try JSONSerialization.data(withJSONObject: arrRecords, options: .prettyPrinted)
            self.writeDataOnDirectory(data: data)
            
        } catch let error {
            print(error.localizedDescription)
            self.delegate?.didFailToSaveDataOnDirectory(error: error)
        }
    }
    
    func removeData(dictData: Dictionary<String, Any>)-> Void {
        
        self.deleteFileFromUrl(dictData: dictData)
    }
    
    func removeAllRecords() {
        
        let urls = self.getListOfFileURLFromDictory(dicrectoryName: KDirectoryName_Sensor)
        if urls.count > 0 {
            
            for url in urls {
                deleteDate(url: url)
            }
        }
    }
}

extension FileManagerProcessor {
    
                    //MARK: Private
    private  func createDirectoryWithNameIfNeeded(folderName: String) {
        
        var documentsPath = self.getDocumentDirectoryPath()
        if documentsPath != nil {
            documentsPath?.appendPathComponent(folderName)
        }
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: (documentsPath?.path)!) {
            print("Folder already exist...")
            
        } else {
            
            do {
                _ =  try fileManager.createDirectory(atPath: (documentsPath?.path)!, withIntermediateDirectories: true, attributes: nil)
                
            } catch let error {
                
                print("Unable to create directory",error.localizedDescription)
            }
        }
    }
    
    private  func getDocumentDirectoryPath() -> URL? {
        
        let path = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: KAppGroup)
        return path
    }
    
    private func writeDataOnDirectory(data: Data) {
        
        
        let timeStamp = CommonHelper.getCurrentTimeStamp()
        let fileName = String(timeStamp) + ".dat"
        
        var documentsPath = self.getDocumentDirectoryPath()
        
        if documentsPath != nil {
            documentsPath?.appendPathComponent(KDirectoryName_Sensor)
            documentsPath?.appendPathComponent(fileName)
        }
        
        let blockOperation = BlockOperation()
        blockOperation.addExecutionBlock({
            
            do {
                _ = try data.write(to: documentsPath!)
                self.delegate?.didsaveDataOnDirectory()
                
            } catch let error {
                self.delegate?.didFailToSaveDataOnDirectory(error: error)
            }
        })
        self.saveDataOperationSerailQueue.addOperation(blockOperation)
    }
    
    
    private func deleteFileFromUrl(dictData: Dictionary<String, Any>) {
        
        if let url = dictData[KLocalFileURL] as? URL {
            let blockOperation = BlockOperation()
            blockOperation.addExecutionBlock {
                
                do {
                    _ = try FileManager.default.removeItem(at: url)
                    self.delegate?.didDataRemoved(dictData: dictData)
                } catch let error {
                    self.delegate?.didDataRemovedFailed(error: error)
                }
            }
            self.removeDataOperationSerailQueue.addOperation(blockOperation)
        }
    }
    
    private func getListOfFileURLFromDictory(dicrectoryName: String) -> [URL] {
        
        let fileManager = FileManager.default
        var documentsPath = self.getDocumentDirectoryPath()
        
        if documentsPath != nil {
            documentsPath?.appendPathComponent(dicrectoryName)
            
            do {
                let directoryContents = try fileManager.contentsOfDirectory(at: documentsPath!, includingPropertiesForKeys: nil, options: [])
                return directoryContents
            } catch {
                
                 return []
            }
        }
       return []
    }
    
    private func getDataFromUrl(arrUrl: [URL], onCompletionHandler:@escaping (_ arrDictData:[Dictionary<String, Any>])->Void) {
        
        var arrData = [Dictionary<String, Any>]()
        let group = DispatchGroup()
        
        for url in arrUrl {
            
            let blockOperation = BlockOperation()
            group.enter()
            blockOperation.addExecutionBlock({
                
                do {
                    let data = try Data.init(contentsOf: url)
                    
                        var dict = Dictionary<String, Any>()
                        dict[KDATA] = data
                        dict[KLocalFileURL] = url
                        arrData.append(dict)
                        group.leave()
                    
                } catch {
                    group.leave()
                }
            })
            
            self.getDataOperationSerailQueue.addOperation(blockOperation)
        }
        
        group.notify(queue: DispatchQueue.main) {
            
            onCompletionHandler(arrData)
        }
    }
    
    private func deleteDate(url: URL) {
        
        let blockOperation = BlockOperation()
        blockOperation.addExecutionBlock {
            
            do {
                _ = try FileManager.default.removeItem(at: url)
            } catch let error {
                
                print(error.localizedDescription)
            }
        }
        self.removeDataOperationSerailQueue.addOperation(blockOperation)
    }
}
