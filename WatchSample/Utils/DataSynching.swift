//
//  DataSynching.swift
//  WatchSample
//
//  Created by Deepak on 11/16/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData

class DataSynching: NSObject {

    var synch_inProcess = false
    
    static let shared: DataSynching = {
    return DataSynching()
    }()
    
    
    func synchDataWith_server() -> Void {
        
        Timer.scheduledTimer(withTimeInterval: 180, repeats: true) { (timer) in
            
            print("Start Data Synching...")
        }
    }
    
    func synchDataWith_iPhoneIfNeeded() {
        
        if synch_inProcess == false {
            synch_inProcess = true
            self.nowSynchWith_iPhone()
        }
    }
}

extension DataSynching {
    
    //MARK: Private

    private func nowSynchWith_iPhone() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            
            
            let moc = CoreDataManager.sharedInstance.mainQueueCtxt
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AllData")
            fetchRequest.fetchLimit = 100
            let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            do {
                
                let arr = try moc?.fetch(fetchRequest) as! [AllData]
                if arr.count > 0 {
                   
                    self.sendDataTo_iPhone(arrRecords: arr)
                    self.nowSynchWith_iPhone()
                } else {
                    self.synch_inProcess = false
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    private func sendDataTo_iPhone(arrRecords: [AllData]) {
        
        let arrDict = self.getDictInfo(arrRecords: arrRecords)
        for dictInfo in arrDict {
         DeviceConnectivity.shared.sendData_iPhone(dictInfo: dictInfo)
        }
    }
    
    private func getDictInfo(arrRecords: [AllData]) -> [Dictionary<String, Any>] {
        
        var arrDict = [Dictionary<String, Any>]()
        for record in arrRecords {
            var dictInfo = Dictionary<String, Any>()
            dictInfo[kActivity] = CommonHelper.convertToString(obj: record.activity)
            dictInfo[kAltitude] = CommonHelper.convertToString(obj: record.altitude)
            dictInfo[KCadence] = CommonHelper.convertToString(obj: record.cadence)
            dictInfo[KDistance] = CommonHelper.convertToString(obj: record.distance)
            dictInfo[KFlightAsc] = CommonHelper.convertToString(obj: record.flightAsc)
            dictInfo[KFlightdsc] = CommonHelper.convertToString(obj: record.flightdsc)
            dictInfo[KHeadingAngle] = CommonHelper.convertToString(obj: record.headingAngle)
            dictInfo[KHeartRate] = CommonHelper.convertToString(obj: record.heartRate)
            dictInfo[kAltitude] = CommonHelper.convertToString(obj: record.latitude)
            dictInfo[KLongitude] = CommonHelper.convertToString(obj: record.longitude)
            dictInfo[KPace] = CommonHelper.convertToString(obj: record.pace)
            dictInfo[kPresure] = CommonHelper.convertToString(obj: record.presure)
            dictInfo[KRotationAngle] = CommonHelper.convertToString(obj: record.rotationAngle)
            dictInfo[KSessionId] = record.sessionId
            dictInfo[KSteps] = CommonHelper.convertToString(obj: record.steps)
            dictInfo[KTimeStamp] = record.timeStamp
            dictInfo[KxAcceleration] = CommonHelper.convertToString(obj: record.xAcceleration)
            dictInfo[KyAcceleration] = CommonHelper.convertToString(obj: record.yAcceleration)
            dictInfo[KzAcceleration] = CommonHelper.convertToString(obj: record.zAcceleration)
            dictInfo[Kxrotation] = CommonHelper.convertToString(obj: record.xRotation)
            dictInfo[Kyrotation] = CommonHelper.convertToString(obj: record.yRotation)
            dictInfo[Kzrotation] = CommonHelper.convertToString(obj: record.zRotation)
            dictInfo[KxQuaternion] = CommonHelper.convertToString(obj: record.xQuaternion)
            dictInfo[KyQuaternion] = CommonHelper.convertToString(obj: record.yQuaternion)
            dictInfo[KzQuaternion] = CommonHelper.convertToString(obj: record.zQuaternion)

            arrDict.append(dictInfo)
            
            CoreDataManager.sharedInstance.mainQueueCtxt?.delete(record)
        }
        do {
            _ = try         CoreDataManager.sharedInstance.mainQueueCtxt?.save()

        } catch let error {
            print(error.localizedDescription)
        }
        return arrDict
    }
    
    
    
    private func stopPreviouseTimer() {
        
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(makeConnectionTrust), object: nil)
        self.timer_connectionCheck?.invalidate()
        self.timer_connectionCheck = nil
    }
    
}
