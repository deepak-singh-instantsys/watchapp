//
//  AllData+CoreDataProperties.swift
//  WatchSample
//
//  Created by Deepak on 09/01/18.
//  Copyright © 2018 Deepak. All rights reserved.
//
//

import Foundation
import CoreData


extension AllData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AllData> {
        return NSFetchRequest<AllData>(entityName: "AllData")
    }

    @NSManaged public var activity: String?
    @NSManaged public var altitude: String?
    @NSManaged public var cadence: String?
    @NSManaged public var distance: String?
    @NSManaged public var flight_asc: String?
    @NSManaged public var flight_dsc: String?
    @NSManaged public var heading_angle: String?
    @NSManaged public var heart_rate: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
    @NSManaged public var pace: String?
    @NSManaged public var presure: String?
    @NSManaged public var rotation_angle: String?
    @NSManaged public var steps: String?
    @NSManaged public var temperature: String?
    @NSManaged public var time_stamp: Double
    @NSManaged public var x_quaternion: String?
    @NSManaged public var y_quaternion: String?
    @NSManaged public var z_quaternion: String?
    @NSManaged public var w_quaternion: String?
    @NSManaged public var x_acceleration: String?
    @NSManaged public var y_acceleration: String?
    @NSManaged public var z_acceleration: String?
    @NSManaged public var x_rotationRate: String?
    @NSManaged public var y_rotationRate: String?
    @NSManaged public var z_rotationRate: String?
    @NSManaged public var x_user_accel: String?
    @NSManaged public var y_user_accel: String?
    @NSManaged public var z_user_accel: String?

}
