//
//  ICloudeDesboardViewController.swift
//  WatchSample
//
//  Created by Deepak on 24/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData
import CloudKit
import HealthKit

class ICloudeDesboardViewController: UIViewController {
    
    let healthStore = HKHealthStore()
    let mainQ = OperationQueue.main
    var startDate = Date()
    var arrData = [AllData]()
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnCSV: UIButton!
    @IBOutlet weak var lblTotalRecords: UILabel!
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.requestForDataFromCloud()
        self.getData()
        //self.requestForHealthKit()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.iCloudLogin()
        btnCSV.setTitle("Generate .csv File", for: .normal)
        btnCSV.isEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        self.requestForHealthKit()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
    }
    
    //MARK: IBActions
    
    @IBAction func tapDelete(_ sender: Any) {
        
        
        SwiftHelper.showAlertOnActiveController(title: KAPPName, cancelButtonTitle: "No", otherButtonTitle: "Yes", message: "Do you want to delete?") { (index) in
            if index == 1 {
                self.deleteRecordFromLocalDb()
            }
        }
    }
    
    
    
    @IBAction func tapCSV(_ sender: Any) {
        
        if arrData.count > 0 {
            btnCSV.setTitle("Please wait, it will take more time than usual.", for: .normal)
            btnCSV.isEnabled = false
            GenerateCSV.shared.generateAllDataCSV(fromViewC: self)
            
        } else {
            SwiftHelper.showOkAlertFor(title: "Data not Available", message: "Data synching process is going on, if data available on cloud, please wait...", obj: self, completion: { (index) in
                
            })
        }
    }
}

extension ICloudeDesboardViewController {
    
    //MARK: Private
    
    private func iCloudLogin() {
        
        CloudKitManager.shared.checkLoginStatus { (success) in
            if !success {
                
                SwiftHelper.showOkAlertFor(title: "iCloud Login", message: "Please login to iCloud in your phone.", obj: self, completion: { (index) in
                    self.lblStatus.text = "Please login into cloud in your phone."
                })
            }
        }
    }
    
    @objc private func getData() {
        
        let moc = CoreDataManager.sharedInstance.mainQueueCtxt
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AllData")
        let sortDescriptor = NSSortDescriptor(key: Ktime_stamp, ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            
            var arr = try moc?.fetch(fetchRequest) as! [AllData]
            if arr.count > 0 {
                
                self.arrData = arr
                mainQ.addOperation({
                    self.lblTotalRecords.text = String.init(format: "%d",arr.count)
                })
            } else {
                
                  arr.removeAll()
                mainQ.addOperation({
                    self.lblTotalRecords.text = String.init(format: "%d",arr.count)
                })
            }
            
        } catch let error {
            self.showStatus(msg: error.localizedDescription)
        }
        
        self.showStatus(msg: "Fetching data from local DB completed.")
        
    }
    
    
    @objc private func requestForDataFromCloud() {
        
        self.showStatus(msg: "trying to fetch data from cloud")
        CloudKitManager.shared.fetchRecord { (records, error) in
            
            if error == nil {
                
                let arrDictInfo = CloudKitManager.getArrayOfDictionaryInfo(records: records)
                if arrDictInfo.count > 0 {
                    
                    self.showStatus(msg: "saving data in local Db in process")
                    CoreDataStorage.shared.saveRecordsInBatch(arrRecords: arrDictInfo, onCompletion: {
                        
                        self.getDataTimer()
                        self.deleteRecordsFromCloud(records: records)
                    })
                } else {
                    
                    self.showStatus(msg: "data not available on cloud")
                    
                    self.timerForICloud()
                }
            } else {
                
                self.showStatus(msg: "data not available on cloud")
                self.timerForICloud()
            }
        }
    }
    
    private func showStatus(msg: String) {
        
        if CommonHelper.isStringValid(string: msg) {
            DispatchQueue.main.async {
                
                self.lblStatus.text = msg.capitalized
            }
        }
    }
    
    private func getDataTimer() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            
            self.getData()
        })
    }
    
    private func deleteRecordFromLocalDb() {
        
        CoreDataStorage.shared.deleteAllRecords { (success) in
            if success {
                self.getData()
            }
        }
    }
    
    private func deleteRecordsFromCloud(records: [CKRecord]) {
        
        CloudKitManager.shared.deleteBatchRecords(records: records, onBatchCompletion: { [weak self] in
            self?.showStatus(msg: "Batch remove from cloud completed")
            self?.timerForICloud()
        })
    }
    
    private func timerForICloud() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0, execute: {
            
            self.requestForDataFromCloud()
        })
    }
    
    private func getInfoFromRecord(records: [CKRecord]) -> [Dictionary<String, Any>]{
        
        if records.count > 0 {
            
        }
        return []
    }
}

extension ICloudeDesboardViewController {
    //MARK: HealthKit
    
    private func requestForHealthKit() {
        
        let shareType = self.dataTypesToWrite()
        let readType = self.dataTypesToRead()
        healthStore.requestAuthorization(toShare: shareType, read: readType) { (success, error) -> Void in
            if !success {
                
                print("Error when requesting authorization: \(String(describing: error))")
            }
        }
    }
    
    private func dataTypesToWrite() -> Set<HKSampleType> {
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let workoutObjectType = HKObjectType.workoutType()
        return  [heartRateQuantityType, workoutObjectType]
    }
    
    private func dataTypesToRead() -> Set<HKObjectType> {
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let workoutObjectType = HKObjectType.workoutType()
        return  [heartRateQuantityType, workoutObjectType]
    }
}

