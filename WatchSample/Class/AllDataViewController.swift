//
//  AllDataViewController.swift
//  WatchSample
//
//  Created by Deepak on 11/17/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import CoreData
import CloudKit

class AllDataViewController: UIViewController {
    
    
    var authReceivedACk = false
    var progressValue = 0.0
    @IBOutlet weak var lblAuthStatus: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var viewServerRecords: UIView!
    @IBOutlet weak var lblTotalServerRecords: UILabel!
    
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.progressViewAnimation()
        self.sendAuth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.checkAuthStatus()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
    }
    
    //MARK: IBActions
}

extension AllDataViewController {
    
    //MARK: Public
    
    public func didReceivAuthAck() {
        
        DispatchQueue.main.async {
            
            self.checkAuthStatus()
        }
    }
    
    public func didSensorObservationReceived(dictInfo: Dictionary<String, Any>) {
        
       /* DispatchQueue.main.async {
             let count = CommonHelper.convertToString(obj: dictInfo[KTOTAL_UPLOAD_COUNT])
            let unprocessDataCount = CommonHelper.convertToString(obj: dictInfo[KMAX_LIMIT_UNPROCESS_DATA_COUNT])
            
            let value: Double = Double(count)! * Double(unprocessDataCount)!
            self.lblTotalServerRecords.text = String.init(format: "%.1f", value)
        }*/
    }
}

extension AllDataViewController {

    private func checkAuthStatus() {
        
        let auth = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KWATCH_AUTH))
        if auth == "1" {
            
            authReceivedACk = true
            self.btnStatus.isHidden = false
            self.viewServerRecords.isHidden = false
            self.progressView.isHidden = true
            self.lblAuthStatus.isHidden = true
            
        } else {
            
            authReceivedACk = false
            self.btnStatus.isHidden = true
            self.progressView.isHidden = false
            self.lblAuthStatus.isHidden = false
            self.viewServerRecords.isHidden = true

        }
    }
    
    private func sendAuthentication(username: String, password: String) {
      
        print("sendAuthentication")
        let dictAuth = [KMESSAGE_TYPE : KMESSAGE_TYPE_AUTH, KUSERNAME : username, KPASSWORD: password]
        KSharedAppDelegate.sendDictInfo(dictInfo: dictAuth)
    }
    
    private func sendAuth() {
        
        if authReceivedACk == false {
            
            let userName = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KUSERNAME))
            let password = CommonHelper.convertToString(obj: KUserDefault.value(forKey: KPASSWORD))
            
            self.sendAuthentication(username: userName, password: password)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            self.sendAuth()
        }
    }
    
    private func progressViewAnimation() {
        
        if self.progressValue >= 1 {
            self.progressValue = 0
        }
        
        self.progressValue += 0.1
        
        if authReceivedACk == false {
            
            UIView.animate(withDuration: 0.23,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            
                            self.progressView.progress = Float(self.progressValue)
                            self.view.layoutIfNeeded()
                            
            }, completion: { (finished) -> Void in
                
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.progressViewAnimation()
        }
    }
}
