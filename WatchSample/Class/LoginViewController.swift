//
//  LoginViewController.swift
//  WatchSample
//
//  Created by Deepak on 08/12/17.
//  Copyright © 2017 Deepak. All rights reserved.
//

import UIKit
import HealthKit


class LoginViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        self.requestForHealthKit()
        self.requestForLocationAccess()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func tapLogin(_ sender: Any) {
        
//        if CommonHelper.isStringValid(string: self.txtPassword.text) &&  CommonHelper.isStringValid(string: self.txtUserName.text) {
//
//            let user = txtUserName.text!
//            let password = txtPassword.text!
//
//            /* let plainString = "\(user):\(password)" as NSString
//             let plainData = plainString.data(using: String.Encoding.utf8.rawValue)
//             let base64String = plainData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//             let autorization = "Basic " + base64String
//             let headers = ["Authorization": autorization]
//             print(headers)*/
//
//            self.activityIndicator.startAnimating()
//            self.btnLogin.isEnabled = false
//            APIClient.shared.login(userName: user, password: password, onCompletion: { (dictRes) in
//
//                DispatchQueue.main.async(execute: {
//
//                     self.btnLogin.isEnabled = true
//                    self.activityIndicator.stopAnimating()
//                    guard let dict = dictRes as? Dictionary<String, Any> else {
//                        return
//                    }
//
//                    if let authenticated = dict["authenticated"] as? Int, authenticated == 1 {
//
//                        KUserDefault.setValue("1", forKey: KLogedIn)
//                        KUserDefault.setValue(user, forKey: KUSERNAME)
//                        KUserDefault.setValue(password, forKey: KPASSWORD)
//                        KUserDefault.setValue("0", forKey: KWATCH_AUTH)
//                        KUserDefault.synchronize()
//
//                        KSharedAppDelegate.showHomeViewC()
//                    }
//                })
//            })
//        }
    }
}


extension LoginViewController {
    //MARK: Private
    
    private func setupView() {
        
        self.activityIndicator.stopAnimating()
        self.viewEmail.layer.cornerRadius = 6
        self.viewEmail.clipsToBounds = true
        self.viewEmail.layer.borderWidth = 1
        self.viewEmail.layer.borderColor = UIColor.black.cgColor
        
        self.viewPassword.layer.cornerRadius = 6
        self.viewPassword.clipsToBounds = true
        self.viewPassword.layer.borderWidth = 1
        self.viewPassword.layer.borderColor = UIColor.black.cgColor
        
         self.btnLogin.layer.cornerRadius = 6
         self.btnLogin.layer.borderWidth = 1
        self.btnLogin.layer.borderColor = UIColor.black.cgColor

    }
    
    
    private func requestForHealthKit() {
        
        let healthStore = HKHealthStore()
        let readTypes = self.dataTypesToRead()
        let shareTypes = self.dataTypesToWrite()
        
        healthStore.requestAuthorization(toShare: shareTypes, read: readTypes) { (succes, error) in
            
        }
    }
    
    private func dataTypesToWrite() -> Set<HKSampleType> {
        
        let workoutObjectType = HKObjectType.workoutType()
        
        return [workoutObjectType]
    }
    
    private func dataTypesToRead() -> Set<HKObjectType> {
        
        let   heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        return  [heartRateQuantityType]
    }
    
    private func requestForLocationAccess() {
        
        LocationManager.shared.getUserCurrentLocation(onSucessBlok: { (lat, long) in
            
            print(lat,long)
            
        }, onFailedBlok: {
            
        })
    }
    
}
